ALTER TABLE users ADD vender_sin_stock boolean NOT NULL DEFAULT 0 AFTER caja_id;
ALTER TABLE users ADD editar_precio boolean NOT NULL DEFAULT 0 AFTER vender_sin_stock;
