<?php

namespace App\Http\Controllers\Api\Inventario\Salidas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Inventario\Salidas\Salida;
use App\Models\Inventario\Salidas\Detalle;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Kardex;
use App\Models\Inventario\Inventario;
use App\Models\Admin\Empresa;
use Illuminate\Support\Facades\DB;

class SalidasController extends Controller
{
    

    public function index() {
       
        $salidas = Salida::orderBy('id','desc')->paginate(7);

        return Response()->json($salidas, 200);

    }


    public function read($id) {

        $salida = Salida::where('id', $id)->with('detalles')->firstOrFail();
        return Response()->json($salida, 200);

    }
    
    public function search($txt) {

        $salidas = Salida::whereHas('cliente', function($query) use ($txt) {
                        $query->where('nombre', 'like' ,'%' . $txt . '%');
                    })->paginate(7);

        return Response()->json($salidas, 200);

    }

    public function filter(Request $request) {


        $salidas = Salida:://whereBetween('created_at', [$star, $end])
                            when($request->inicio, function($query) use ($request){
                                return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                            })
                            ->when($request->usuario_id, function($query) use ($request){
                                return $query->where('usuario_id', $request->usuario_id);
                            })
                            ->when($request->estado, function($query) use ($request){
                                return $query->where('estado', $request->estado);
                            })
                            ->when($request->tipo, function($query) use ($request){
                                return $query->where('bodega_id', $request->tipo);
                            })
                            ->orderBy('id','desc')->paginate(100000);

        return Response()->json($salidas, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'fecha'         => 'required',
            'bodega_id'     => 'required|numeric',
            'concepto'      => 'required|max:255',
            'detalles'      => 'required|array',
            'usuario_id'     => 'required|numeric',
        ]);

        DB::beginTransaction();
         
        try {

            if($request->id)
                $salida = Salida::findOrFail($request->id);
            else
                $salida = new Salida;

            $salida->fill($request->all());
            $salida->save();

            // Detalles
            foreach ($request->detalles as $value) {
                if (!isset($value['id'])) {
                    $detalle = new Detalle;
                    $value['salida_id'] = $salida->id;
                    $detalle->fill($value);
                    $detalle->save();
                }
            }

            // Afectar Inventario
            // if ($request->estado == 'Aprobado') {
                foreach ($request->detalles as $value) {
                    // Actualizar inventario
                        $producto = Producto::findOrFail($value['producto_id']);

                        // Disminuir inventario
                        $inventario = Inventario::where('producto_id', $producto->id)->where('bodega_id', $salida->bodega_id)->first();
                        if ($inventario) {
                            $inventario->stock -= $value['cantidad'];
                            $inventario->save();
                            $inventario->kardex($salida, $value['cantidad']);
                        }


                }
            // }

        DB::commit();
        return Response()->json($salida, 200);

        } catch (\Exception $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        } catch (\Throwable $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        }

        return Response()->json($salida, 200);

    }

    public function delete($id)
    {
        $salida = Salida::findOrFail($id);
        $salida->delete();

        return Response()->json($salida, 201);

    }

    public function generarDoc($id) {

        $salida = Salida::where('id', $id)->with('detalles')->firstOrFail();
        $empresa = Empresa::find(1);

        $reportes = \PDF::loadView('reportes.inventario.salida', compact('salida', 'empresa'));
        return $reportes->stream();

    }


}
