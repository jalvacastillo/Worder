<?php

namespace App\Http\Controllers\Api\Inventario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Empresa;
use App\Models\Admin\Sucursal;
use App\Models\Inventario\Categorias\SubCategoria;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Ajuste;
use App\Models\Inventario\Inventario;
use App\Models\Inventario\Bodega;
use App\Models\Inventario\Sucursal as ProductoSucursal;
use App\Models\Compras\Compra;
use App\Models\Compras\Detalle as DetalleCompra;
use App\Models\Ventas\Venta;
use App\Models\Ventas\Detalle as DetalleVenta;
use App\Imports\Productos;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

class ProductosController extends Controller
{
    

    public function index() {
       
        $productos = Producto::where('tipo', 'Producto')->with('inventarios', 'sucursales')
                                // ->whereNull('codigo')
                                ->orderBy('id','desc')->paginate(10);

        return Response()->json($productos, 200);

    }

    public function list() {
       
        $productos = Producto:://where('tipo', 'Producto')->
                                orderby('nombre')->get();

        return Response()->json($productos, 200);

    }


    public function porCodigo($codigo) {
       
        $producto = Producto:://where('tipo', 'Producto')->
                                    where('codigo', $codigo )
                                    ->wherehas('sucursales', function($q){
                                        $q->where('sucursal_id', \JWTAuth::parseToken()->authenticate()->sucursal_id)
                                            ->where('activo', true);
                                    })
                                    ->with('inventarios', 'precios')->get();

        return Response()->json($producto, 200);

    }

    public function read($id) {

        $producto = Producto::where('id', $id)
                                ->with('inventarios', 'composiciones', 'precios', 'promociones', 'imagenes', 'sucursales')
                                ->first();
        return Response()->json($producto, 200);

    }

    public function search($txt) {

        $productos = Producto:://whereIn('tipo', ['Producto', 'Servicio'])->
                                with('inventarios', 'precios')
                                ->where('nombre', 'like' ,'%' . $txt . '%')
                                ->wherehas('sucursales', function($q){
                                    $q->where('sucursal_id', \JWTAuth::parseToken()->authenticate()->sucursal_id)
                                        ->where('activo', true);
                                })
                                ->orwhere('codigo', 'like' ,"%" . $txt . "%")
                                ->paginate(10);
        return Response()->json($productos, 200);

    }

    public function searchAll($txt) {

        $productos = Producto::whereIn('tipo', ['Producto', 'Repuesto'])->with('inventarios')
                                ->where('nombre', 'like' ,'%' . $txt . '%')
                                ->orwhere('codigo', 'like' ,'%' . $txt . '%')
                                ->paginate(10);
        return Response()->json($productos, 200);

    }

    public function filter(Request $request) {

            $productos = Producto::where('tipo', 'Producto')->with('inventarios', 'sucursales')
                                ->when($request->categoria_id, function($query) use ($request){
                                    return $query->where('categoria_id', $request->categoria_id);
                                })
                                ->when($request->subcategoria_id, function($query) use ($request){
                                    return $query->where('subcategoria_id', $request->subcategoria_id);
                                })
                                ->when($request->stock_bodega, function($query) use ($request){
                                    return $query->where('inventario', true)->whereHas('inventarios', function($query){
                                        return $query->where('bodega_id', 1)->whereRaw('stock <= stock_min');
                                    });
                                })
                                ->when($request->stock_venta, function($query) use ($request){
                                    return $query->where('inventario', true)->whereHas('inventarios', function($query){
                                        return $query->where('bodega_id', 2)->whereRaw('stock <= stock_min');
                                    });
                                })
                                ->when($request->sin_control_inventario, function($query) use ($request){
                                    return $query->where('inventario', false);
                                })
                                ->when($request->sin_subcategoria, function($query) use ($request){
                                    return $query->whereNull('subcategoria_id');
                                })
                                ->when($request->sin_condigo, function($query) use ($request){
                                    return $query->whereNull('codigo');
                                })
                                ->orderBy('id','desc')->paginate(100000);

            return Response()->json($productos, 200);
    }

    public function store(Request $request)
    {
        if(empty($request->codigo)){
            $request['codigo'] = NULL;
        }

        $request->validate([
            'nombre'    => 'required|max:255',
            // 'codigo'    => 'nullable|unique:productos,codigo,'. $request->id,
            'precio'    => 'required|numeric',
            'costo'     => 'required|numeric',
            'medida'     => 'required',
            'categoria_id' => 'required',
            'subcategoria_id' => 'required',
            'empresa_id'    => 'required',
        ]);

        if($request->id)
            $producto = Producto::findOrFail($request->id);
        else
            $producto = new Producto;
        

        $producto->fill($request->all());
        $producto->save();

        // Configurar inventarios para las sucursales
        if (!$request->id && $producto->tipo != 'Servicio') {
            $sucursales = Sucursal::all();
            foreach ($sucursales as $sucursal) {
                $sucur = new ProductoSucursal;
                $sucur->producto_id    = $producto->id;
                $sucur->activo    = true;
                $sucur->sucursal_id      = $sucursal->id;
                $sucur->save();
            }

            $bodegas = Bodega::all();

            foreach ($bodegas as $bodega) {
                $inventario = new Inventario;
                $inventario->producto_id    = $producto->id;
                $inventario->stock          = 0;
                $inventario->stock_min      = 0;
                $inventario->stock_max      = 100;
                $inventario->bodega_id      = $bodega->id;
                $inventario->save();
            }
        }


        return Response()->json($producto, 200);

    }

    public function storeDesdeCompras(Request $request)
    {
        if(empty($request->codigo)){
            $request['codigo'] = NULL;
        }

        $request->validate([
            'nombre'    => 'required|max:255',
            // 'codigo'    => 'nullable|unique:productos,codigo,'. $request->id,
            'precio'    => 'required|numeric',
            'costo'     => 'required|numeric',
            'medida'     => 'required',
            'categoria_id' => 'required',
            'subcategoria_id' => 'required',
            'empresa_id'    => 'required',
        ]);

        if($request->id)
            $producto = Producto::where('tipo', 'Producto')->findOrFail($request->id);
        else
            $producto = new Producto;
        
        $producto->fill($request->all());
        $producto->save();

        $sucursales = \App\Models\Admin\Sucursal::all();

        foreach ($sucursales as $sucursal) {
            $producto_sucursal = new \App\Models\Inventario\Sucursal();
            $producto_sucursal->producto_id = $producto->id;
            // $producto_sucursal->inventario = true;
            // $producto_sucursal->bodega_venta_id = $sucursal->bodegas()->first()->id;
            $producto_sucursal->activo = true;
            $producto_sucursal->sucursal_id = $sucursal->id;
            $producto_sucursal->save();


            $inventario = new Inventario;
            $inventario->producto_id = $producto->id;
            $inventario->stock = 0;
            $inventario->stock_min = 10;
            $inventario->stock_max = 100;
            $inventario->nota = '';
            $inventario->bodega_id = $sucursal->bodegas()->first()->id;
            // $inventario->sucursal_id = $producto_sucursal->id;
            $inventario->save();
            
        }

        $producto = Producto::where('tipo', 'Producto')->where('id', $producto->id)->with('inventarios')->first();

        return Response()->json($producto, 200);

    }

    public function delete($id)
    {
        $producto = Producto::findOrFail($id);
        $producto->inventarios()->delete();
        $producto->delete();

        return Response()->json($producto, 201);

    }

    public function precios($id)
    {
        $producto = Producto::findOrFail($id);
        
        
        $ventas = DetalleVenta::where('producto_id', $producto->id)->get();

        $ventas_precios =  collect();
        $ventas_fechas =  collect();

        foreach ($ventas->unique('precio') as $venta) {
            $ventas_precios->push($venta->precio);
            $ventas_fechas->push($venta->created_at->format('d/m/Y'));
        }
        $producto->ventas_precios = $ventas_precios;
        $producto->ventas_fechas = $ventas_fechas;
        $producto->ventas = count($ventas);

        return Response()->json($producto, 201);

    }


    public function analisis(Request $request) {


            $productos = Producto::where('tipo', 'Producto')->when($request->nombre, function($query) use ($request){
                                        return $query->where('nombre', 'like' ,'%' . $request->nombre . '%');
                                    })
                                    ->when($request->categoria_id, function($query) use ($request){
                                        return $query->where('categoria_id', $request->categoria_id);
                                    })

                                    ->get();

            $movimientos = collect();

            $empresa = Empresa::find(1);

            foreach ($productos as $producto) {
                if ($empresa->valor_inventario == 'Promedio') {
                    $producto->costo = $producto->costo_promedio;
                }
                $utilidad = $producto->precio - $producto->costo;
                $margen = $producto->costo > 0 ? (round($utilidad / $producto->costo, 2) * 100) : null;
                $movimientos->push([
                    'nombre'        => $producto->nombre,
                    'nombre_categoria'        => $producto->nombre_categoria,
                    'nombre_subcategoria'        => $producto->nombre_subcategoria,
                    // 'proveedor'     => $producto->proveedor,
                    'precio'        => $producto->precio,
                    'costo'         => $producto->costo,
                    'utilidad'      => $utilidad,
                    'margen'        =>  $margen
                ]);
            }

            return Response()->json($movimientos, 200);
    }

    public function compras(Request $request, $id) {

        $compras = Compra::whereHas('detalles', function($q) use ($id) {
                                    $q->where('producto_id', $id);
                                })
                                ->orderBy('id','desc')->paginate(5);
        

        return Response()->json($compras, 200);

    }

    public function ajustes(Request $request, $id) {

        $ajustes = Ajuste::where('producto_id', $id)->orderBy('id','desc')->paginate(5);
        
        return Response()->json($ajustes, 200);

    }

    public function ventas(Request $request, $id) {

        $ventas = Venta::whereHas('detalles', function($q) use ($id) {
                                    $q->where('producto_id', $id);
                                })
                                ->orderBy('id','desc')->paginate(5);
        
        return Response()->json($ventas, 200);

    }

    public function vendedor() {
       
        $productos = Producto::where('tipo', 'Producto')->with('inventarios', 'sucursales')
                                // ->whereNull('codigo')
                                ->orderBy('id','desc')->paginate(12);

        return Response()->json($productos, 200);

    }

    public function vendedorBuscador($txt) {
       
        $productos = Producto::whereIn('tipo', ['Producto', 'Servicio'])->with('inventarios')
                                ->where('nombre', 'like' ,'%' . $txt . '%')
                                ->orwhere('codigo', 'like' ,'%' . $txt . '%')
                                ->paginate(12);
        return Response()->json($productos, 200);

    }


    public function import(Request $request){
        
        $request->validate([
            'file'          => 'required',
        ]);

        $import = new Productos();
        Excel::import($import, $request->file);
        
        return Response()->json($import->getRowCount(), 200);

    }

    public function export(Request $request){

      $productos = new ProductosExport();
      $productos->filter($request);

      return Excel::download($productos, 'productos.xlsx');
    }

}
