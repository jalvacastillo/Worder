<?php

namespace App\Http\Controllers\Api\Inventario\Entradas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Inventario\Entradas\Entrada;
use App\Models\Inventario\Entradas\Detalle;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Kardex;
use App\Models\Inventario\Inventario;
use App\Models\Admin\Empresa;
use Illuminate\Support\Facades\DB;

class EntradasController extends Controller
{
    

    public function index() {
       
        $entradas = Entrada::orderBy('id','desc')->paginate(7);

        return Response()->json($entradas, 200);

    }


    public function read($id) {

        $entrada = Entrada::where('id', $id)->with('detalles')->firstOrFail();
        return Response()->json($entrada, 200);

    }
    
    public function search($txt) {

        $entradas = Entrada::whereHas('cliente', function($query) use ($txt) {
                        $query->where('nombre', 'like' ,'%' . $txt . '%');
                    })->paginate(7);

        return Response()->json($entradas, 200);

    }

    public function filter(Request $request) {


        $entradas = Entrada:://whereBetween('created_at', [$star, $end])
                            when($request->inicio, function($query) use ($request){
                                return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                            })
                            ->when($request->usuario_id, function($query) use ($request){
                                return $query->where('usuario_id', $request->usuario_id);
                            })
                            ->when($request->estado, function($query) use ($request){
                                return $query->where('estado', $request->estado);
                            })
                            ->when($request->tipo, function($query) use ($request){
                                return $query->where('bodega_id', $request->tipo);
                            })
                            ->orderBy('id','desc')->paginate(100000);

        return Response()->json($entradas, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'fecha'         => 'required',
            'bodega_id'     => 'required|numeric',
            'concepto'      => 'required|max:255',
            'detalles'      => 'required|array',
            'usuario_id'     => 'required|numeric',
        ]);

        DB::beginTransaction();
         
        try {

            if($request->id)
                $entrada = Entrada::findOrFail($request->id);
            else
                $entrada = new Entrada;

            $entrada->fill($request->all());
            $entrada->save();

            // Detalles
            foreach ($request->detalles as $value) {
                if (!isset($value['id'])) {
                    $detalle = new Detalle;
                    $value['entrada_id'] = $entrada->id;
                    $detalle->fill($value);
                    $detalle->save();
                }
            }

            // Afectar Inventario
            // if ($request->estado == 'Aprobado') {
                foreach ($request->detalles as $value) {
                    // Actualizar inventario
                        $producto = Producto::findOrFail($value['producto_id']);

                        // Aumentar inventario
                        $inventario = Inventario::where('producto_id', $producto->id)->where('bodega_id', $entrada->bodega_id)->first();
                        if ($inventario) {
                            $inventario->stock += $value['cantidad'];
                            $inventario->save();
                            $inventario->kardex($entrada, $value['cantidad']);
                        }


                }
            // }

        DB::commit();
        return Response()->json($entrada, 200);

        } catch (\Exception $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        } catch (\Throwable $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        }

        return Response()->json($entrada, 200);

    }

    public function delete($id)
    {
        $entrada = Entrada::findOrFail($id);
        $entrada->delete();

        return Response()->json($entrada, 201);

    }

    public function generarDoc($id) {

        $entrada = Entrada::where('id', $id)->with('detalles')->firstOrFail();
        $empresa = Empresa::find(1);

        $reportes = \PDF::loadView('reportes.inventario.entrada', compact('entrada', 'empresa'));
        return $reportes->stream();

    }


}
