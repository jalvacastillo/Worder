<?php

namespace App\Http\Controllers\Api\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ventas\Abono;
use App\Models\Ventas\Venta;
use Illuminate\Support\Facades\DB;

class AbonosController extends Controller
{
    

    public function index() {
       
        $abono = Abono::orderBy('id','desc')->paginate(10);
        return Response()->json($abono, 200);

    }


    public function read($id) {

        $abono = Abono::findOrFail($id);
        return Response()->json($abono, 200);

    }

    public function store(Request $request)
    {

        $request->validate([
            'fecha'       => 'required|date',
            'concepto'    => 'required|max:255',
            'estado'      => 'required|max:255',
            'metodo_pago' => 'required|max:255',
            'total'       => 'required|numeric',
            'venta_id'    => 'required|numeric',
            'cliente_id'    => 'required|numeric',
            'usuario_id'    => 'required|numeric',
            'sucursal_id'    => 'required|numeric',
        ],[
            'venta_id.required' => 'Seleccione la venta a abonar',
            'cliente_id.required' => 'Seleccione un cliente',
        ]);

        DB::beginTransaction();
         
        try {
            // Actualizar el saldo de la venta
                $venta = Venta::find($request->venta_id);
                
                if (!$venta) {
                    return  Response()->json(['error' => 'Venta no encontrada', 'code' => 400], 400);
                }

                $nuevoSaldo = $venta->saldo - $request->total;

                if ($nuevoSaldo < 0) {
                    return  Response()->json(['error' => 'El monto del abono es mayor que el saldo actual de la venta', 'code' => 400], 400);
                }
                $venta->saldo = $nuevoSaldo;
                if ($venta->saldo == 0) {
                    $venta->estado = 'Pagada';
                    $venta->fecha_pago = date('Y-m-d');
                }
                $venta->save();

            $abono = new Abono;
            $abono->fill($request->all());
            $abono->save();


        DB::commit();
        return Response()->json($abono, 200);

        } catch (\Exception $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        } catch (\Throwable $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        }

    }

    public function delete($id){
        $abono = Abono::findOrFail($id);

            $venta = Venta::find($abono->venta_id);
            $venta->saldo = $venta->saldo + $abono->total;
            if ($venta->saldo > 0) {
                $venta->estado = 'Pendiente';
            }
            $venta->save();

        $abono->delete();
        
        return Response()->json($abono, 201);

    }

    public function generarDoc($id) {

        $abono = Abono::where('id', $id)->with('venta', 'cliente')->firstOrFail();

        $reportes = \PDF::loadView('reportes.facturacion.abono', compact('abono'));
        return $reportes->stream();

    }


}
