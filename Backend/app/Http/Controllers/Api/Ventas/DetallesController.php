<?php

namespace App\Http\Controllers\Api\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Ventas\Detalle;
use App\Models\Inventario\Producto;
use App\Models\Admin\Bomba;
use App\Models\Admin\Tanque;
use App\Models\Inventario\Inventario;

class DetallesController extends Controller
{
    

    public function index() {

        $detalles = Detalle::orderBy('created_at','desc')->paginate(10);

        return Response()->json($detalles, 200);

    }


    public function read($id) {

        $detalle = Detalle::findOrFail($id);
        return Response()->json($detalle, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'producto_id'    => 'required',
            'cantidad'    => 'required',
            'precio'    => 'required',
            'costo'    => 'required',
            'venta_id'    => 'required'
        ]);
        
        if($request->id){
            $detalle = Detalle::findOrFail($request->id);
        }
        else{
            $detalle = new Detalle;

        // Actualizar inventario
            $producto = Producto::findOrFail($request->producto_id);
            if ($producto->inventario) {
                // Si es gasolina disminuir tanque
                if ($producto->categoria_id == 1) {
                    $bomba = Bomba::findOrFail($request->bomba_id);
                    
                    $tanque = Tanque::findOrFail($bomba->tanque_id);
                    $tanque->stock -= $request->cantidad;
                    $bomba->lectura += $request->cantidad;
                    $bomba->save();
                    $tanque->save();
                // Si es producto disminuir bodega
                } else {
                    $bodega = Inventario::where('bodega_id', 2)->where('producto_id',$producto->id)->first();
                    $bodega->stock -= $request->cantidad;
                    $bodega->save();
                }            
            }
        }
        
        $detalle->fill($request->all());
        $detalle->save();

        return Response()->json($detalle, 200);

    }

    public function delete($id)
    {
        $detalle = Detalle::findOrFail($id);
        // Actualizar inventario
            $producto = Producto::findOrFail($detalle->producto_id);
            if ($producto->inventario) {
                Inventario::where('bodega_id', $detalle->venta->bodega_id)->where('producto_id', $detalle->producto_id)->increment('stock', $detalle->cantidad);
            }
        $detalle->delete();

        return Response()->json($detalle, 201);

    }

    public function historial(Request $request) {

        $ventas = Detalle::whereHas('venta', function($query) use ($request){
                            $query->where('estado', 'Pagada')
                            ->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->nombre, function($query) use ($request){
                            return $query->whereHas('producto', function($q) use ($request){
                                $q->where('nombre', 'like' ,'%' . $request->nombre . '%');
                            });
                        })
                        ->when($request->categoria_id, function($query) use ($request){
                            return $query->whereHas('producto', function($q) use ($request){
                                $q->where('categoria_id', $request->categoria_id );
                            });
                        })
                        ->get()
                        ->groupBy('producto_id');

        
        $movimientos = collect();

        foreach ($ventas as $detalles) {
            $total = $detalles->sum('total');
            $iva = $detalles->sum('iva');
            $ventaNeta = $total - $iva;
            $costoTotal = $detalles->sum('subcosto');
            $utilidad = $ventaNeta - $costoTotal;
            $movimientos->push([
                'fecha'         => $detalles[0]->venta->fecha,
                'nombre_producto'      => $detalles[0]->producto()->pluck('nombre')->first(),
                'medida'        => $detalles[0]->producto()->pluck('medida')->first(),
                'precio'        => $detalles->avg('precio'), // Hay varios precios
                'costo'         => $detalles->avg('costo'), // Hay varios costos
                'cantidad'      => $detalles->sum('cantidad'),
                'total'         => $total,
                'iva'           => $iva,
                'ventaNeta'     => $ventaNeta,
                'costoTotal'    => $costoTotal,
                'utilidad'      => $utilidad,
                'margen'        => $total > 0 ? round( ( $utilidad / $ventaNeta * 100), 2) : null
            ]);
        }

        $movimientos = $movimientos->sortBy('nombre_producto')->values()->all();

        return Response()->json($movimientos, 200);

    }


}
