<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use stdClass;

use App\Models\Admin\Empresa;
use App\Models\Admin\Caja;
use App\Models\Admin\Corte;

use App\Models\Ventas\Venta;
use App\Models\Ventas\Detalle as VentaDetalle;
use App\Models\Ventas\Devoluciones\Devolucion as DevolucionVenta;
use App\Models\Ventas\Clientes\Cliente;
use App\Models\Ventas\Abono;

use App\Models\Compras\Compra;
use App\Models\Compras\Devoluciones\Devolucion as DevolucionCompra;
use App\Models\Compras\Gastos\Gasto;

use App\Models\Inventario\Producto;

use App\Models\User;

class DashController extends Controller
{

    public function index(Request $request) {

        $datos = new stdClass();

        // Cajas
            $cajas = Caja::get();
            
            foreach ($cajas as $caja) {
                $caja->ventas_suma = $caja->ventasDia()->sum('total');
                if ($caja->corte()->first()) {
                    $caja->estado = $caja->corte()->first()->estado;
                }else{
                    $caja->estado = 'Sin Cortes';
                }
                $caja->ventas_cantidad = $caja->ventasDia()->count();
            }

            $datos->cajas = $cajas;


        // Ventas
            $ventas                 = Venta::whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            })
                                            ->get();
            $abonos                 = Abono::whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            })
                                            ->get();

            $datos->total_ventas    = $ventas->whereBetween('fecha_pago', [$request->inicio, $request->fin])->where('estado', 'Pagada')->sum('total');
            $datos->total_ventas_pendientes    = $ventas->where('estado', 'Pendiente')->sum('total');

            $ventas_devoluciones    = DevolucionVenta::whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            })
                                            ->get();
            $datos->total_ventas_devoluciones    = $ventas_devoluciones->sum('total');
            $datos->total_abonos    = $abonos->sum('total');

        // Egresos
            $compras = Compra::whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            })
                                            ->get();

            $datos->total_compras   = $compras->where('estado', 'Pagada')->sum('total');
            $datos->total_compras_pendientes       = $compras->where('estado', 'Pendiente')->sum('total');

            $compras_devoluciones    = DevolucionCompra::whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            })
                                            ->get();
            $datos->total_compras_devoluciones    = $compras_devoluciones->sum('total');

            $gastos                 = Gasto::whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            })
                                            ->get();

            $datos->total_gastos    = $gastos->where('estado', 'Pagado')->sum('total');
            $datos->total_gastos_pendientes    = $gastos->where('estado', 'Pendiente')->sum('total');



        // Productos

            $datos->productos   = VentaDetalle::
                                        selectRaw('sum(cantidad) AS total, producto_id, (select nombre from productos where producto_id = id) as nombre')
                                        ->groupBy('producto_id')
                                        ->whereHas('venta', function($q) use($request){
                                            $q->whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            });
                                        })
                                        ->orderBy('total', 'desc')
                                        ->take(5)
                                        ->get();

        // Balances
            $datos->total_ingresos = $datos->total_ventas - $datos->total_ventas_devoluciones;
            $datos->total_egresos = $datos->total_compras - $datos->total_compras_devoluciones + $datos->total_gastos;
            $datos->total_cxc = $datos->total_ventas_pendientes;
            $datos->total_cxp = $datos->total_compras_pendientes + $datos->total_gastos_pendientes;
            $datos->total_balance = $datos->total_ingresos - $datos->total_egresos;

            $datos->total_utilidad = $ventas->where('estado', 'Pagada')->sum('subtotal') - $ventas_devoluciones->sum('subtotal');


        return Response()->json($datos, 200);
    }

    public function vendedor() {

        $datos = new stdClass();

        $usuario_id = \JWTAuth::parseToken()->authenticate()->id;

        $ordenes                = Venta::where('usuario_id', $usuario_id)
                                        ->whereMonth('fecha', date('m'))
                                        ->whereYear('fecha', date('Y'))
                                        ->where('estado', '!=', 'Cancelada')->get();
                         
        $datos->cantidad_ordenes    = $ordenes->count();
        $datos->suma_ordenes        = $ordenes->sum('total');
                            
        $datos->tclientes        = Cliente::whereHas('ordenes', function($q) use($usuario_id){
                                            $q->whereMonth('fecha', date('m'))->whereYear('fecha', date('Y'))
                                            ->where('estado', '!=', 'Cancelada')
                                            ->where('usuario_id', $usuario_id);
                                        })->count();
                            
        $datos->tproductos        = VentaDetalle::whereHas('venta', function($q) use($usuario_id){
                                            $q->whereMonth('fecha', date('m'))->whereYear('fecha', date('Y'))
                                            ->where('estado', '!=', 'Cancelada')
                                            ->where('usuario_id', $usuario_id);
                                        })->count();


        return Response()->json($datos, 200);
    }

    public function cajero($id){

        // $datos = new stdClass();
        $usuario = User::findOrFail($id);
        $caja    = Caja::where('id', $usuario->caja_id)->with('corte')->firstOrFail();
        
        // Sino hay corte se muestran los datos del dia.
        if ($caja->corte) {
            $ordenes     = Venta::where('estado', 'En Proceso')
                                    // ->where('fecha', '>=', Carbon::parse($caja->corte->apertura)->format('Y-m-d'))
                                    ->paginate(10000);
        } else {
            $ordenes     = Venta::where('estado', 'En Proceso')
                                    ->where('fecha', '>=', Carbon::today())
                                    ->paginate(10000);
        }
        
        // Se saca el total de venta
        // $datos->ventas              = $ventas->sum('total');
        // $datos->ventas_efectivo     = $ventas->where('forma_de_pago','Efectivo')->sum('total');
        // $datos->ventas_tarjeta      = $ventas->where('forma_de_pago','Tarjeta')->sum('total');
        // $datos->ventas_vale         = $ventas->where('forma_de_pago','Vale')->sum('total');
        // $datos->ventas_cheque       = $ventas->where('forma_de_pago','Cheque')->sum('total');
        // $datos->ventas_versatec     = $ventas->where('forma_de_pago','Versatec')->sum('total');

        return Response()->json($ordenes, 200);
    }

    public function barcode($codigo) {
        
        return view('reportes.barcode', compact('codigo'));

        
        $reportes = \PDF::loadView('reportes.barcode', compact('codigo'))->setPaper('letter');
        return $reportes->stream();

    }



}
