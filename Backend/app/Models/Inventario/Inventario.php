<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;
use App\Models\Inventario\Kardex;

class Inventario extends Model {

    protected $table = 'producto_inventarios';
    protected $fillable = array(
        'producto_id',
        'stock',
        'stock_min',
        'stock_max',
        'nota',
        'bodega_id'
    );

    protected $appends = ['nombre_bodega', 'nombre_sucursal'];

    public function getNombreBodegaAttribute(){
        return $this->bodega()->pluck('nombre')->first();
    }

    public function getNombreSucursalAttribute(){
        return $this->bodega()->first()->nombre_sucursal;
    }

    public function kardex($modelo, $cantidad){

        $clase = get_class($modelo);

        $entradaCantidad =  null;
        $salidaCantidad =  null;

        if ($clase == 'App\Models\Ventas\Venta') { //Salida
            if ($cantidad > 0) {
                $salidaCantidad =  $cantidad;
                $clase = 'Venta';
            }else{
                $entradaCantidad =  abs($cantidad);
                $clase = 'Venta Anulada';
            }
        }
        if ($clase == 'App\Models\Transporte\Mantenimientos\Mantenimiento') { //Mantenimiento
            $salidaCantidad =  $cantidad;
            $clase = 'Mantenimiento';
        }
        else if ($clase == 'App\Models\Compras\Compra') {
            if ($cantidad > 0) {
                $entradaCantidad =  $cantidad;
                $clase = 'Compra';
            }else{
                $salidaCantidad =  abs($cantidad);
                $clase = 'Compra Anulada';
            }
        }
        else if ($clase == 'App\Models\Inventario\Ajuste') {
            if ($cantidad > 0) {
                $entradaCantidad =  $cantidad;
            }else{
                $salidaCantidad =  abs($cantidad);
            }
            $clase = 'Ajuste';
        }
        else if ($clase == 'App\Models\Inventario\Entradas\Entrada') {
            if ($cantidad > 0) {
                $entradaCantidad =  $cantidad;
                $clase = 'Otra Entrada';
            }else{
                $salidaCantidad =  abs($cantidad);
                $clase = 'Otra Entrada Anulada';
            }
        }
        else if ($clase == 'App\Models\Inventario\Salidas\Salida') {
            if ($cantidad > 0) {
                $salidaCantidad =  $cantidad;
                $clase = 'Otra Salida';
            }else{
                $entradaCantidad =  abs($cantidad);
                $clase = 'Otra Salida Anulada';
            }
        }
        else if ($clase == 'App\Models\Inventario\Traslados\Traslado') {
            if ($cantidad > 0) {
                if ($modelo->estado == 'Cancelado') {
                    $clase = 'Cancelación de traslado de ' . $modelo->destino()->pluck('nombre')->first();
                    $salidaCantidad =  $cantidad;
                }else{
                    $entradaCantidad =  $cantidad;
                    $clase = 'Traslado de ' . $modelo->origen()->pluck('nombre')->first();
                }
            }else{
                if ($modelo->estado == 'Cancelado') {
                    $clase = 'Cancelación de traslado a ' . $modelo->origen()->pluck('nombre')->first();
                    $entradaCantidad =  abs($cantidad);
                }else{
                    $salidaCantidad =  abs($cantidad);
                    $clase = 'Traslado a ' . $modelo->destino()->pluck('nombre')->first();
                }
            }
        }
        else if ($clase == 'App\Models\Ventas\Devoluciones\Devolucion') {
            if ($cantidad > 0) {
                $entradaCantidad =  $cantidad;
                $clase = 'Devolución Venta';
            }else{
                $salidaCantidad =  abs($cantidad);
                $clase = 'Devolución Venta Anulada';
            }
        }
        else if ($clase == 'App\Models\Compras\Devoluciones\Devolucion') {
            if ($cantidad > 0) {
                $salidaCantidad =  $cantidad;
                $clase = 'Devolución Compra';
            }else{
                $entradaCantidad =  abs($cantidad);
                $clase = 'Devolución Compra Anulada';
            }
        }else{
            // return null;
        }

        $precio = $this->producto()->pluck('precio')->first();
        $costo = $this->producto()->pluck('costo')->first();

        Kardex::create([
            'fecha'             => date('Y-m-d'),
            'producto_id'       => $this->producto_id,
            'bodega_id'         => $this->bodega_id,
            'detalle'           => $clase,
            'referencia'        => $modelo->id,
            'precio_unitario'   => $salidaCantidad ? $precio : null,
            'costo_unitario'    => $entradaCantidad ? $costo : null,
            'entrada_cantidad'  => $entradaCantidad,
            'entrada_valor'     => $entradaCantidad ? $entradaCantidad * $costo : null,
            'salida_cantidad'   => $salidaCantidad,
            'salida_valor'      => $salidaCantidad ? $salidaCantidad * $precio : null,
            'total'             => $this->stock,
            'usuario_id'        => $modelo->usuario_id,
        ]);
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

    public function bodega(){
        return $this->belongsTo('App\Models\Inventario\Bodega', 'bodega_id');
    }

}



