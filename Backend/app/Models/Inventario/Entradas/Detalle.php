<?php

namespace App\Models\Inventario\Entradas;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model {

    protected $table = 'inventario_entrada_detalles';
    protected $fillable = array(
        'producto_id',
        'cantidad',
        'costo',
        'total',
        'entrada_id',
    );

    protected $appends = ['nombre_producto'];

    public function getNombreProductoAttribute(){
        return $this->producto()->pluck('nombre')->first();
    }

    public function entrada(){
        return $this->belongsTo('App\Models\Inventario\Entradas\Entrada', 'entrada_id');
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

}
