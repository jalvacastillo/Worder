<?php

namespace App\Models\Inventario\Entradas;

use Illuminate\Database\Eloquent\Model;

class Entrada extends Model {

    protected $table = 'inventario_entradas';
    protected $fillable = array(
        'fecha',
        'bodega_id',
        'concepto',
        'tipo',
        'usuario_id'
    );

    protected $appends = ['nombre_usuario', 'nombre_bodega'];

    public function getNombreUsuarioAttribute(){
        return $this->usuario()->pluck('name')->first();
    }

    public function getNombreBodegaAttribute(){
        return $this->bodega()->pluck('nombre')->first();
    }

    public function detalles(){
        return $this->hasMany('App\Models\Inventario\Entradas\Detalle', 'entrada_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }

    public function bodega(){
        return $this->belongsTo('App\Models\Inventario\Bodega', 'bodega_id');
    }

}



