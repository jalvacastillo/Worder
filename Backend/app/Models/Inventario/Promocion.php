<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Promocion extends Model {

    protected $table = 'producto_promociones';
    protected $fillable = array(
        'producto_id',
        'precio',
        'inicio',
        'fin'
    );

    protected $appends = ['activa', 'nombre_producto', 'nombre_categoria', 'nombre_subcategoria', 'precio_final'];

    public function getActivaAttribute($value)
    {
        if ($this->fin >= Carbon::now())
            return true;
        else
            return false;
    }

    public function getNombreProductoAttribute($value)
    {
        return $this->producto()->pluck('nombre')->first();
    }

    public function getPrecioFinalAttribute()
    {
        $impuesto = $this->producto()->pluck('impuesto')->first();
        if ($impuesto > 0)
            return number_format($this->precio + $this->precio * $impuesto, 2);
        return number_format($this->precio);
    }

    public function getNombreCategoriaAttribute($value)
    {
        return $this->producto()->first()->categoria()->pluck('nombre')->first();
    }

    public function getNombreSubcategoriaAttribute($value)
    {
        return $this->producto()->first()->subcategoria()->pluck('nombre')->first();
    }

    public function getInicioAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function getFinAttribute($value)
    {
        return Carbon::parse($value);
    }
   
    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto','producto_id');
    }

}



