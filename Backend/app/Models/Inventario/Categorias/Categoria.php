<?php

namespace App\Models\Inventario\Categorias;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';
    protected $fillable = array(
        'nombre',
        'img',
        'descripcion',
        'empresa_id'
    );


    public function subcategorias(){
        return $this->hasMany('App\Models\Inventario\Categorias\SubCategoria', 'categoria_id');
    }

    public function productos(){
        return $this->hasMany('App\Models\Inventario\Producto', 'categoria_id');
    }

}
