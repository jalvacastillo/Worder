<?php

namespace App\Models\Inventario\Salidas;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model {

    protected $table = 'inventario_salida_detalles';
    protected $fillable = array(
        'producto_id',
        'cantidad',
        'costo',
        'total',
        'salida_id',
    );

    protected $appends = ['nombre_producto'];

    public function getNombreProductoAttribute(){
        return $this->producto()->pluck('nombre')->first();
    }

    public function salida(){
        return $this->belongsTo('App\Models\Inventario\Salidas\Salida', 'salida_id');
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

}
