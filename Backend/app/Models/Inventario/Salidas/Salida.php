<?php

namespace App\Models\Inventario\Salidas;

use Illuminate\Database\Eloquent\Model;

class Salida extends Model {

    protected $table = 'inventario_salidas';
    protected $fillable = array(
        'fecha',
        'bodega_id',
        'concepto',
        'tipo',
        'usuario_id'
    );

    protected $appends = ['nombre_usuario', 'nombre_bodega'];

    public function getNombreUsuarioAttribute(){
        return $this->usuario()->pluck('name')->first();
    }

    public function getNombreBodegaAttribute(){
        return $this->bodega()->pluck('nombre')->first();
    }

    public function detalles(){
        return $this->hasMany('App\Models\Inventario\Salidas\Detalle', 'salida_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }

    public function bodega(){
        return $this->belongsTo('App\Models\Inventario\Bodega', 'bodega_id');
    }

}



