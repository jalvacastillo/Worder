<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Precio extends Model
{
    protected $table = 'producto_precios';
    protected $fillable = [
        'precio',
        'producto_id',
    ];

    // protected static function booted()
    // {
    //     $usuario = Auth::user();

    //     if ($usuario){

    //         if ($usuario->tipo == 'Ventas') {
    //             static::addGlobalScope('permiso', function (Builder $builder) use ($usuario) {
    //                 $builder->whereHas('usuarios', function($q) use ($usuario){
    //                         return $q->where('id_usuario', $usuario->id);
    //                     });
    //             });
    //         }
    //     }
        
    // }

    protected $appends = ['precio_final'];

    public function getPrecioFinalAttribute()
    {
        return number_format($this->precio + ($this->precio * $this->producto->impuesto), 2);
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

    public function usuarios(){
        return $this->hasMany('App\Models\PrecioUsuario', 'id_precio');
    }

}
