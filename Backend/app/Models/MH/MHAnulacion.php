<?php

namespace App\Models\MH;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class MHAnulacion extends Model
{

    public $venta;
    public $caja;
    public $caja_codigo;
    public $empresa;
    

    public function generarDTE($venta, $DTE){
        $this->venta = $venta;
        $this->empresa = $this->venta->empresa()->first();
        $codigoGeneracion = strtoupper(Uuid::uuid4()->toString());
        $this->caja_codigo = '0001';

        $identificacion = [
            "version" => 2,
            "ambiente" => $DTE['identificacion']['ambiente'],
            "codigoGeneracion" => $codigoGeneracion,
            "fecAnula" => \Carbon\Carbon::now()->format('Y-m-d'),
            "horAnula" => \Carbon\Carbon::now()->format('H:i:s'),
        ];

        $tipo_documento = NULL;
        $num_documento = NULL;

        $nombre = null;
        $correo = null;
        $telefono = null;


        if ($DTE['identificacion']['tipoDte'] == '01') {
            $tipo_documento = $DTE['receptor']['tipoDocumento'];
            $num_documento = $DTE['receptor']['numDocumento'];

            $nombre = $DTE['receptor']['nombre'];
            $correo = $DTE['receptor']['correo'];
            $telefono = $DTE['receptor']['telefono'];
        }

        if ($DTE['identificacion']['tipoDte'] == '03') {
            $tipo_documento = '36';
            $num_documento = $DTE['receptor']['nit'];
            $nombre = $DTE['receptor']['nombre'];
            $correo = $DTE['receptor']['correo'];
            $telefono = $DTE['receptor']['telefono'];
        }

        if ($DTE['identificacion']['tipoDte'] == '14') {
            $tipo_documento = $DTE['sujetoExcluido']['tipoDocumento'];
            $num_documento = $DTE['sujetoExcluido']['numDocumento'];
            $nombre = $DTE['sujetoExcluido']['nombre'];
            $correo = $DTE['sujetoExcluido']['correo'];
            $telefono = $DTE['sujetoExcluido']['telefono'];
        }

        $documento = [
            "tipoDte" => $DTE['identificacion']['tipoDte'],
            "codigoGeneracion" => $DTE['identificacion']['codigoGeneracion'],
            "selloRecibido" => $DTE['sello'],
            "numeroControl" => $DTE['identificacion']['numeroControl'],
            "fecEmi" => $DTE['identificacion']['fecEmi'],
            "montoIva" => isset($this->venta->iva) ? floatval(number_format($this->venta->iva, 2, '.', '')) : NULL,
            "codigoGeneracionR" => NULL, // Solo si el motivo es error, hay que mandar el que sustituye
            "tipoDocumento" => $tipo_documento,
            "numDocumento" => $num_documento,
            "nombre" => $nombre,
            "correo" => $correo,
            "telefono" => $telefono,
        ];

        // 1. Error en la Información del Documento Tributario Electrónico a invalidar.
        // 2. Rescindir de la operación realizada.
        // 3. Otro.

        $motivo = [
            "tipoAnulacion" => 2,
            "motivoAnulacion" => 'Se rescinde la operación.',
            "nombreResponsable" => $DTE['emisor']['nombre'],
            "tipDocResponsable" => '36',
            "numDocResponsable" => $DTE['emisor']['nit'],
            "nombreSolicita" => $DTE['emisor']['nombre'],
            "tipDocSolicita" =>  '36',
            "numDocSolicita" => $DTE['emisor']['nit'],
        ];

        switch ($this->empresa->tipo_establecimiento) {
            case 'Sucursal':
                $this->empresa->tipoEstablecimiento = '01';
                break;
            case 'Casa matriz':
                $this->empresa->tipoEstablecimiento = '02';
                break;
            case 'Bodega':
                $this->empresa->tipoEstablecimiento = '04';
                break;
            default:
                $this->empresa->tipoEstablecimiento = '02';
                break;
        }


        $emisor = [
            "nit" => str_replace('-', '', $this->empresa->nit),
            "nombre" => $this->empresa->nombre,
            "tipoEstablecimiento" => $this->empresa->tipoEstablecimiento,
            "nomEstablecimiento" => $this->empresa->nombre,
            "codEstable" => $this->empresa->cod_estable ? $this->empresa->cod_estable : NULL,
            "codPuntoVenta" => $this->caja_codigo ? $this->caja_codigo : NULL,
            "telefono" => $this->empresa->telefono,
            "correo" => $this->empresa->correo,
        ];

        return  
            [
                "identificacion" => $identificacion,
                "emisor" => $emisor,
                "documento" => $documento,
                "motivo" => $motivo,
            ];

    }


}

