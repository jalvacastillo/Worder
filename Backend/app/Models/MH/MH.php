<?php

namespace App\Models\MH;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Http;
use App\Models\MH\Unidad;

class MH extends Model
{
    // protected $url_firmado = 'http://localhost:8113/firmardocumento/';
    protected $url_firmado = 'http://203.161.49.180:8113/firmardocumento/';
    protected $url_mh = 'https://apitest.dtes.mh.gob.sv/fesv/recepciondte';
    protected $url_anular_dte = 'https://apitest.dtes.mh.gob.sv/fesv/anulardte';
    protected $url_auth = 'https://apitest.dtes.mh.gob.sv/seguridad/auth';

    public $venta;
    public $compra;
    public $caja;
    public $empresa;
    
    public function auth($empresa){
        $this->empresa = $empresa;

        $response = Http::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded', 'User-Agent' => 'Laravel',
        ])->asForm()->post($this->url_auth, [ 'user' => str_replace('-', '', $this->empresa->mh_usuario), 'pwd' => $this->empresa->mh_contrasena ]);

        return $response->json();
    }

    public function firmarDTE($DTE)
    {

        $response = Http::post($this->url_firmado,[
            'nit' => str_replace('-', '', $this->empresa->nit),
            'activo' => true,
            'passwordPri' => $this->empresa->mh_pwd_certificado,
            'dteJson' => $DTE,
        ]);

        return $response->json();

    }

    public function enviarDTE($auth, $DTEFirmado){

        $response = Http::withHeaders(['Content-Type' => 'application/JSON', 'User-Agent' => 'Laravel', 'Authorization' => $auth['body']['token'],
        ])->post($this->url_mh, [
            'ambiente' => $this->venta->ambiente,
            'idEnvio' => $this->venta->correlativo,
            'version' => $this->venta->version,
            'tipoDte' => $this->venta->tipo_dte,
            'documento' => $DTEFirmado['body'],
            'codigoGeneracion' => $this->venta->codigo_generacion
        ]);

        return $response->json();

    }   

    public function generarDTE($venta){
        $this->venta = $venta;
        $this->caja = $this->venta->caja()->first();
        $this->empresa = $this->venta->empresa()->first();

        if ($this->venta->tipo_documento == 'Credito Fiscal') {
            $this->venta->tipo_dte = '03';
        }

        if ($this->venta->tipo_documento == 'Factura'){
            $this->venta->tipo_dte = '01';
        }

        if (!$this->venta->codigo_generacion) {
            $this->venta->numero_control = 'DTE-'. $this->venta->tipo_dte . '-' . $this->empresa->cod_estable_mh . $this->caja->codigo . '-' .str_pad($this->venta->correlativo, 15, '0', STR_PAD_LEFT);
            $this->venta->codigo_generacion = strtoupper(Uuid::uuid4()->toString());
            $this->venta->save();
        }

        $this->venta->ambiente = $this->empresa->fe_ambiente; // 00 Modo prueba 01 Modo producción
        $this->venta->tipoModelo = 1; // 1 Modelo Facturación previo 2 Modelo Facturación diferido
        $this->venta->tipoOperacion = 1; // 1 Transmisión normal 2 Transmisión por contingencia
        $this->venta->tipoContingencia = NULL; // 1 No disponibilidad de sistema del MH 2 No disponibilidad de sistema del emisor 3 Falla en el suministro de servicio de Internet del Emisor 4 Falla en el suministro de servicio de energía eléctrica del emisor que impida la transmisión de los DTE 5 Otro (deberá digitar un máximo de 500 caracteres explicando el motivo)
        $this->venta->motivoContin = NULL;
        $this->venta->moneda = 'USD';

        if ($this->venta->tipo_dte == '03') {
            $this->venta->version = 3;
        }

        if ($this->venta->tipo_dte == '01'){
            $this->venta->version = 1;
        }


        // Condición
            if ($this->venta->condicion == 'Crédito'){
                $this->venta->cod_condicion = 2; //Credito
            }else{
                $this->venta->cod_condicion = 1; //Contado
            }

        // Metodo de pago
            switch ($this->venta->metodo_pago) {
                case 'Efectivo': //Billetes y monedas
                    $this->venta->cod_metodo_pago = '01';
                    break;
                case 'Tarjeta': //Tarjeta Débito y Credito
                    $this->venta->cod_metodo_pago = '02';
                    break;
                case 'Cheque': //Tarjeta Débito
                    $this->venta->cod_metodo_pago = '04';
                    break;
                case 'Transferencia': //Transferencia_ Depósito Bancario
                    $this->venta->cod_metodo_pago = '05';
                    break;
                case 'Vales': //Vales o Cupones
                    $this->venta->cod_metodo_pago = '06';
                    break;
                case 'Chivo Wallet': //Dinero electrónico
                    $this->venta->cod_metodo_pago = '09';
                    break;
                case 'Bitcoin': //Dinero electrónico
                    $this->venta->cod_metodo_pago = '11';
                    break;
                default:
                    $this->venta->cod_metodo_pago = '01';
                    break;
            }

        // Total en letras
        $partes = explode('.', strval( number_format($this->venta->total, 2, '.', '') ));
        $this->venta->total_en_letras = \NumeroALetras::convertir($partes[0], 'DOLARES CON ') . $partes[1].'/100';


        // 01 Factura
        // 03 Comprobante de crédito fiscal
        // 04 Nota de remisión 05 Nota de crédito 06 Nota de débito
        // 07 Comprobante de retención 08 Comprobante de liquidación 09 Documento contable de liquidación 11 Facturas de exportación 14 Factura de sujeto excluido 15 Comprobante de donación

        if ($this->venta->tipo_dte == '01') {
            return $this->generarFactura();
        }
        if ($this->venta->tipo_dte == '03') {
            return $this->generarCCF();
        }

    }

    public function generarDTESujetoExcluido($compra){
        $this->compra = $compra;
        $this->empresa = $this->compra->empresa()->first();

        if ($this->compra->tipo_documento == 'Sujeto Excluido') {
            $this->compra->tipo_dte = '14';
        }

        if (!$this->compra->codigo_generacion) {
            $this->compra->numero_control = 'DTE-'. $this->compra->tipo_dte . '-' . $this->empresa->cod_estable_mh . '0001-' .str_pad($this->compra->referencia, 15, '0', STR_PAD_LEFT);
            $this->compra->codigo_generacion = strtoupper(Uuid::uuid4()->toString());
            $this->compra->save();
        }

        $this->compra->ambiente = $this->empresa->fe_ambiente; // 00 Modo prueba 01 Modo producción
        $this->compra->tipoModelo = 1; // 1 Modelo Facturación previo 2 Modelo Facturación diferido
        $this->compra->tipoOperacion = 1; // 1 Transmisión normal 2 Transmisión por contingencia
        $this->compra->tipoContingencia = NULL; // 1 No disponibilidad de sistema del MH 2 No disponibilidad de sistema del emisor 3 Falla en el suministro de servicio de Internet del Emisor 4 Falla en el suministro de servicio de energía eléctrica del emisor que impida la transmisión de los DTE 5 Otro (deberá digitar un máximo de 500 caracteres explicando el motivo)
        $this->compra->motivoContin = NULL;
        $this->compra->moneda = 'USD';
        $this->compra->version = 1;

        // Condición
            if ($this->compra->condicion == 'Crédito'){
                $this->compra->cod_condicion = 2; //Credito
            }else{
                $this->compra->cod_condicion = 1; //Contado
            }

        // Metodo de pago
            switch ($this->compra->metodo_pago) {
                case 'Efectivo': //Billetes y monedas
                    $this->compra->cod_metodo_pago = '01';
                    break;
                case 'Tarjeta': //Tarjeta Débito y Credito
                    $this->compra->cod_metodo_pago = '02';
                    break;
                case 'Cheque': //Tarjeta Débito
                    $this->compra->cod_metodo_pago = '04';
                    break;
                case 'Transferencia': //Transferencia_ Depósito Bancario
                    $this->compra->cod_metodo_pago = '05';
                    break;
                case 'Vales': //Vales o Cupones
                    $this->compra->cod_metodo_pago = '06';
                    break;
                case 'Chivo Wallet': //Dinero electrónico
                    $this->compra->cod_metodo_pago = '09';
                    break;
                case 'Bitcoin': //Dinero electrónico
                    $this->compra->cod_metodo_pago = '11';
                    break;
                default:
                    $this->compra->cod_metodo_pago = '01';
                    break;
            }

        // Total en letras
        $partes = explode('.', strval( number_format($this->compra->total, 2, '.', '') ));
        $this->compra->total_en_letras = \NumeroALetras::convertir($partes[0], 'DOLARES CON ') . $partes[1].'/100';;


        // 01 Factura
        // 03 Comprobante de crédito fiscal
        // 04 Nota de remisión 05 Nota de crédito 06 Nota de débito
        // 07 Comprobante de retención 08 Comprobante de liquidación 09 Documento contable de liquidación 11 Facturas de exportación 14 Factura de sujeto excluido 15 Comprobante de donación

        if ($this->compra->tipo_dte == '14') {
            return $this->generarSujeroExcluido();
        }

    }  

    public function generarDTEAnulado($venta, $DTE){
        $codigoGeneracion = strtoupper(Uuid::uuid4()->toString());
        $this->caja = $venta->caja()->first();
        $this->empresa = $venta->empresa()->first();

        $identificacion = [
            "version" => 2,
            "ambiente" => $DTE['identificacion']['ambiente'],
            "codigoGeneracion" => $codigoGeneracion,
            "fecAnula" => \Carbon\Carbon::now()->format('Y-m-d'),
            "horAnula" => \Carbon\Carbon::now()->format('H:i:s'),
        ];

        $tipo_documento = NULL;
        $num_documento = NULL;


        if ($DTE['identificacion']['tipoDte'] == '01') {
            $tipo_documento = $DTE['receptor']['tipoDocumento'];
            $num_documento = $DTE['receptor']['numDocumento'];
        }

        if ($DTE['identificacion']['tipoDte'] == '03') {
            $tipo_documento = '36';
            $num_documento = $DTE['receptor']['nit'];
        }

        $documento = [
            "tipoDte" => $DTE['identificacion']['tipoDte'],
            "codigoGeneracion" => $DTE['identificacion']['codigoGeneracion'],
            "selloRecibido" => $DTE['sello'],
            "numeroControl" => $DTE['identificacion']['numeroControl'],
            "fecEmi" => $DTE['identificacion']['fecEmi'],
            "montoIva" => isset($DTE['resumen']['totalIva']) ? $DTE['resumen']['totalIva'] : NULL,
            "codigoGeneracionR" => NULL, // Solo si el motivo es error, hay que mandar el que sustituye
            "tipoDocumento" => $tipo_documento,
            "numDocumento" => $num_documento,
            "nombre" => $DTE['receptor']['nombre'],
            "correo" => $DTE['receptor']['correo'],
            "telefono" => $DTE['receptor']['telefono'],
        ];

        // 1. Error en la Información del Documento Tributario Electrónico a invalidar.
        // 2. Rescindir de la operación realizada.
        // 3. Otro.

        $motivo = [
            "tipoAnulacion" => 2,
            "motivoAnulacion" => 'Se rescinde la operación.',
            "nombreResponsable" => $DTE['emisor']['nombre'],
            "tipDocResponsable" => '36',
            "numDocResponsable" => $DTE['emisor']['nit'],
            "nombreSolicita" => $DTE['emisor']['nombre'],
            "tipDocSolicita" =>  '36',
            "numDocSolicita" => $DTE['emisor']['nit'],
        ];

        switch ($this->empresa->tipo_establecimiento) {
            case 'Sucursal':
                $this->empresa->tipoEstablecimiento = '01';
                break;
            case 'Casa matriz':
                $this->empresa->tipoEstablecimiento = '02';
                break;
            case 'Bodega':
                $this->empresa->tipoEstablecimiento = '04';
                break;
            default:
                $this->empresa->tipoEstablecimiento = '20';
                break;
        }


        $emisor = [
            "nit" => str_replace('-', '', $this->empresa->nit),
            "nombre" => $this->empresa->nombre,
            "tipoEstablecimiento" => $this->empresa->tipoEstablecimiento,
            "nomEstablecimiento" => $this->empresa->nombre_comercial,
            "codEstable" => $this->empresa->cod_estable ? $this->empresa->cod_estable : NULL,
            "codPuntoVenta" => $this->caja->codigo ? $this->caja->codigo : NULL,
            "telefono" => $this->empresa->telefono,
            "correo" => $this->empresa->correo,
        ];

        return  
            [
                "identificacion" => $identificacion,
                "emisor" => $emisor,
                "documento" => $documento,
                "motivo" => $motivo,
            ];

    }

    public function generarContingencia($empresa, $DTEs, $tipo = 3){
        $codigoGeneracion = strtoupper(Uuid::uuid4()->toString());
        $this->empresa = $empresa;

        $identificacion = [
            "version" => 3,
            "ambiente" => $DTEs[0]['identificacion']['ambiente'],
            "codigoGeneracion" => $codigoGeneracion,
            "fTransmision" => \Carbon\Carbon::now()->format('Y-m-d'),
            "hTransmision" => \Carbon\Carbon::now()->format('H:i:s'),
        ];

        $detalles = collect();

        foreach ($DTEs as $index => $DTE) {

            $detalles->push([
                "noItem" => $index + 1,
                "codigoGeneracion" => $DTE['identificacion']['codigoGeneracion'],
                "tipoDoc" => $DTE['identificacion']['tipoDte'],
            ]);
        }

        $tipos = [
            "No disponibilidad de sistema del MH",
            "No disponibilidad de sistema del emisor",
            "Falla en el suministro de servicio de Internet del Emisor",
            "Falla en el suministro de servicio de energía eléctrica del emisor, que impida la transmisión de los DTE",
        ];

        $motivo = [
            "fInicio" => \Carbon\Carbon::parse($DTEs[0]['identificacion']['fecEmi'])->format('Y-m-d'),
            "fFin" => \Carbon\Carbon::parse($DTEs[0]['identificacion']['fecEmi'])->format('Y-m-d'),
            "hInicio" => \Carbon\Carbon::parse($DTEs[0]['identificacion']['horEmi'])->format('H:i:s'),
            "hFin" => \Carbon\Carbon::parse($DTEs[0]['identificacion']['horEmi'])->format('H:i:s'),
            "tipoContingencia" => $tipo,
            "motivoContingencia" => 'No se pudieron emitir los DTEs.',
        ];

        switch ($this->empresa->tipo_establecimiento) {
            case 'Sucursal':
                $this->empresa->tipoEstablecimiento = '01';
                break;
            case 'Casa matriz':
                $this->empresa->tipoEstablecimiento = '02';
                break;
            case 'Bodega':
                $this->empresa->tipoEstablecimiento = '04';
                break;
            default:
                $this->empresa->tipoEstablecimiento = '20';
                break;
        }


        $emisor = [
            "nit" => str_replace('-', '', $this->empresa->nit),
            "nombre" => $this->empresa->nombre,
            "nombreResponsable" => $this->empresa->nombre,
            "tipoDocResponsable" => '13',
            "numeroDocResponsable" => $this->empresa->nit,
            "tipoEstablecimiento" => $this->empresa->tipoEstablecimiento,
            "telefono" => $this->empresa->telefono,
            "correo" => $this->empresa->correo,
        ];

        return  
            [
                "identificacion" => $identificacion,
                "emisor" => $emisor,
                "detalleDTE" => $detalles,
                "motivo" => $motivo,
            ];

    }

    public function generarDTEAnuladoSujetoExcluido($compra, $DTE){
        $codigoGeneracion = strtoupper(Uuid::uuid4()->toString());
        $this->empresa = $compra->empresa()->first();

        $identificacion = [
            "version" => 2,
            "ambiente" => $DTE['identificacion']['ambiente'],
            "codigoGeneracion" => $codigoGeneracion,
            "fecAnula" => \Carbon\Carbon::now()->format('Y-m-d'),
            "horAnula" => \Carbon\Carbon::now()->format('H:i:s'),
        ];

        $tipo_documento = NULL;
        $num_documento = NULL;


        if ($DTE['identificacion']['tipoDte'] == '14') {
            $tipo_documento = $DTE['sujetoExcluido']['tipoDocumento'];
            $num_documento = $DTE['sujetoExcluido']['numDocumento'];
        }

        $documento = [
            "tipoDte" => $DTE['identificacion']['tipoDte'],
            "codigoGeneracion" => $DTE['identificacion']['codigoGeneracion'],
            "selloRecibido" => $DTE['sello'],
            "numeroControl" => $DTE['identificacion']['numeroControl'],
            "fecEmi" => $DTE['identificacion']['fecEmi'],
            "montoIva" => isset($DTE['resumen']['totalIva']) ? $DTE['resumen']['totalIva'] : NULL,
            "codigoGeneracionR" => NULL, // Solo si el motivo es error, hay que mandar el que sustituye
            "tipoDocumento" => $tipo_documento,
            "numDocumento" => $num_documento,
            "nombre" => $DTE['sujetoExcluido']['nombre'],
            "correo" => $DTE['sujetoExcluido']['correo'],
            "telefono" => $DTE['sujetoExcluido']['telefono'],
        ];

        // 1. Error en la Información del Documento Tributario Electrónico a invalidar.
        // 2. Rescindir de la operación realizada.
        // 3. Otro.

        $motivo = [
            "tipoAnulacion" => 2,
            "motivoAnulacion" => 'Se rescinde la operación.',
            "nombreResponsable" => $DTE['emisor']['nombre'],
            "tipDocResponsable" => '36',
            "numDocResponsable" => $DTE['emisor']['nit'],
            "nombreSolicita" => $DTE['emisor']['nombre'],
            "tipDocSolicita" =>  '36',
            "numDocSolicita" => $DTE['emisor']['nit'],
        ];

        switch ($this->empresa->tipo_establecimiento) {
            case 'Sucursal':
                $this->empresa->tipoEstablecimiento = '01';
                break;
            case 'Casa matriz':
                $this->empresa->tipoEstablecimiento = '02';
                break;
            case 'Bodega':
                $this->empresa->tipoEstablecimiento = '04';
                break;
            default:
                $this->empresa->tipoEstablecimiento = '20';
                break;
        }


        $emisor = [
            "nit" => str_replace('-', '', $this->empresa->nit),
            "nombre" => $this->empresa->nombre,
            "tipoEstablecimiento" => $this->empresa->tipoEstablecimiento,
            "nomEstablecimiento" => $this->empresa->nombre_comercial,
            "codEstable" => $this->empresa->cod_estable ? $this->empresa->cod_estable : NULL,
            "codPuntoVenta" => NULL,
            "telefono" => $this->empresa->telefono,
            "correo" => $this->empresa->correo,
        ];

        return  
            [
                "identificacion" => $identificacion,
                "emisor" => $emisor,
                "documento" => $documento,
                "motivo" => $motivo,
            ];

    }

    public function anularDTE($auth, $DTE, $DTEFirmado){

        $response = Http::withHeaders(['Content-Type' => 'application/JSON', 'User-Agent' => 'Laravel', 'Authorization' => $auth['body']['token'],
        ])->post($this->url_anular_dte, [
            'ambiente' => $DTE['identificacion']['ambiente'],
            'idEnvio' => $this->venta->id,
            'version' => 2,
            'documento' => $DTEFirmado['body'],
        ]);

        return $response->json();
    }

    protected function identificador(){
        return [
            "version" => $this->venta->version,
            "ambiente" => $this->venta->ambiente,
            "tipoDte" => $this->venta->tipo_dte,
            "numeroControl" => $this->venta->numero_control,
            "codigoGeneracion" => $this->venta->codigo_generacion,
            "tipoModelo" => $this->venta->tipoModelo,
            "tipoOperacion" => $this->venta->tipoOperacion,
            "tipoContingencia" => $this->venta->tipoContingencia,
            "motivoContin" => $this->venta->motivoContin,
            "fecEmi" => \Carbon\Carbon::parse($this->venta->fecha)->format('Y-m-d'),
            "horEmi" => \Carbon\Carbon::parse($this->venta->created_at)->format('H:i:s'),
            "tipoMoneda" => $this->venta->moneda,
        ];
    }

    protected function emisor(){
        
        return [
            "nit" => str_replace('-', '', $this->empresa->nit),
            "nrc" => str_replace('-', '', $this->empresa->nrc),
            "nombre" => $this->empresa->nombre,
            "codActividad" => $this->empresa->cod_actividad_economica,
            "descActividad" => $this->empresa->actividad_economica,
            "nombreComercial" => $this->empresa->nombre_comercial,
            "tipoEstablecimiento" => $this->empresa->tipo_establecimiento,
            "direccion" => [
                "departamento" => $this->empresa->cod_departamento,
                "municipio" => $this->empresa->cod_municipio,
                "complemento" => $this->empresa->direccion,
            ],
            "telefono" => $this->empresa->telefono,
            "codEstableMH" => $this->empresa->cod_estable_mh ? $this->empresa->cod_estable_mh : NULL,
            "codEstable" => $this->empresa->cod_estable ? $this->empresa->cod_estable : NULL,
            "codPuntoVentaMH" => $this->caja->codigo ? $this->caja->codigo : NULL,
            "codPuntoVenta" => $this->caja->codigo ? $this->caja->codigo : NULL,
            "correo" => $this->empresa->correo,
        ];
    }

    protected function receptor(){

        if (!$this->venta->cliente_id) {
            return [
                  "tipoDocumento" => NULL, //36 NIT 13 DUI
                  "numDocumento" => NULL,
                  "nrc" => NULL,
                  "nombre" => 'Consumidor Final',
                  "codActividad" => NULL,
                  "descActividad" => NULL,
                  "direccion" => [
                    "departamento" => $this->empresa->cod_departamento,
                    "municipio" => $this->empresa->cod_municipio,
                    "complemento" => $this->empresa->direccion,
                  ],
                  "telefono" => NULL,
                  "correo" => NULL
                ];
        }

        if ($this->venta->cliente->nit) {
            $this->venta->cliente->tipo_documento = '36';
            $this->venta->cliente->num_documento = $this->venta->cliente->nit ? str_replace('-', '', $this->venta->cliente->nit) : NULL;
        }
        if ($this->venta->cliente->dui) {
            $this->venta->cliente->tipo_documento = '13';
            $this->venta->cliente->num_documento = $this->venta->cliente->dui ? $this->venta->cliente->dui : NULL;
        }

        if ($this->venta->tipo_dte == '01') {
            return [
                  "tipoDocumento" => $this->venta->cliente->tipo_documento, //36 NIT 13 DUI
                  "numDocumento" => $this->venta->cliente->num_documento,
                  "nrc" => NULL,
                  "nombre" => $this->venta->cliente_nombre,
                  "codActividad" => $this->venta->cliente->cod_giro ? $this->venta->cliente->cod_giro : NULL,
                  "descActividad" => $this->venta->cliente->giro ? $this->venta->cliente->giro : NULL,
                  "direccion" => [
                    "departamento" => $this->venta->cliente->cod_departamento,
                    "municipio" => $this->venta->cliente->cod_municipio,
                    "complemento" => $this->venta->cliente->direccion
                  ],
                  "telefono" => $this->venta->cliente->telefono,
                  "correo" => $this->venta->cliente->correo
                ];
        }

        if ($this->venta->tipo_dte == '03') {
            return [
                  "nit" =>  $this->venta->cliente->nit ? str_replace('-', '', $this->venta->cliente->nit) : str_replace('-', '', $this->venta->cliente->dui),
                  "nombreComercial" =>  $this->venta->cliente->nombre_empresa,
                  "nrc" => str_replace('-', '', $this->venta->cliente->registro),
                  "nombre" => $this->venta->cliente_nombre,
                  "codActividad" => $this->venta->cliente->cod_giro,
                  "descActividad" => $this->venta->cliente->giro,
                  "direccion" => [
                    "departamento" => $this->venta->cliente->cod_departamento,
                    "municipio" => $this->venta->cliente->cod_departamento,
                    "complemento" => $this->venta->cliente->direccion
                  ],
                  "telefono" => $this->venta->cliente->telefono,
                  "correo" => $this->venta->cliente->correo
                ];
        }
    }

    public function generarFactura(){
        $tributos = NULL;
        $apendice = NULL;
        
        if ($this->venta->placa || $this->venta->kilometraje || $this->venta->observacion ) {
            $apendice = collect();
            if ($this->venta->placa) {
                $apendice->push(['etiqueta' => 'Placa', 'campo' => 'Placa', 'valor'=> $this->venta->placa]);
            }
            if ($this->venta->kilometraje) {
                $apendice->push(['etiqueta' => 'Kilometraje', 'campo' => 'Kilometraje', 'valor'=> $this->venta->kilometraje]);
            }
            if ($this->venta->observacion) {
                $apendice->push(['etiqueta' => 'Observaciones', 'campo' => 'Observaciones', 'valor'=> $this->venta->observacion]);
            }
        }

        if ($this->venta->fovial > 0 || $this->venta->cotrans > 0) {
            $tributos = collect();
            if ($this->venta->fovial > 0){ 
                $tributos->push(['codigo' => 'D1', 'descripcion'=> 'FOVIAL ($0.20 Ctvs. por galón)', 'valor' => floatval(number_format($this->venta->fovial,2, '.', ''))]);
            }
            if ($this->venta->cotrans > 0){ 
                $tributos->push(['codigo' => 'C8', 'descripcion'=> 'COTRANS ($0.10 Ctvs. por galón)', 'valor' => floatval(number_format($this->venta->cotrans,2, '.', ''))]);
            }
        }

        return 
            [
                "identificacion" => $this->identificador(),
                "documentoRelacionado" => NULL,
                "emisor" => $this->emisor(),
                "receptor" => $this->receptor(),
                "otrosDocumentos" => NULL,
                "ventaTercero" => NULL,
                "cuerpoDocumento" => $this->detallesFactura(),
                "resumen" => [
                  "totalNoSuj" => floatval(number_format($this->venta->no_sujeta, 2, '.', '')),
                  "totalExenta" => floatval(number_format($this->venta->exenta, 2, '.', '')),
                  "totalGravada" => floatval(number_format($this->venta->gravada + $this->venta->iva, 2, '.', '')),
                  "subTotalVentas" => floatval(number_format($this->venta->subtotal + $this->venta->iva, 2, '.', '')),
                  "descuNoSuj" => 0,
                  "descuExenta" => 0,
                  "descuGravada" => floatval(number_format($this->venta->descuento, 2, '.', '')),
                  "porcentajeDescuento" => 0,
                  "totalDescu" => floatval(number_format($this->venta->descuento, 2, '.', '')),
                  "tributos" => $tributos,
                  "subTotal" => floatval(number_format($this->venta->subtotal + $this->venta->iva, 2, '.', '')),
                  "ivaRete1" => floatval(number_format($this->venta->iva_retenido, 2, '.', '')),
                  "reteRenta" => 0,
                  "montoTotalOperacion" => floatval(number_format($this->venta->total, 2, '.', '')),
                  "totalNoGravado" => 0,
                  "totalPagar" => floatval(number_format($this->venta->total, 2, '.', '')),
                  "totalLetras" => $this->venta->total_en_letras,
                  "totalIva" => floatval(number_format($this->venta->iva, 2, '.', '')),
                  "saldoFavor" => 0,
                  "condicionOperacion" => $this->venta->cod_condicion,
                  "pagos" => [
                    [
                      "codigo" => $this->venta->cod_metodo_pago,
                      "montoPago" => floatval(number_format($this->venta->total, 2, '.', '')),
                      "referencia" => NULL,
                      "plazo" => NULL,
                      "periodo" => NULL
                    ]
                  ],
                  "numPagoElectronico" => ""
                ],
                "extension" => NULL,
                "apendice" => $apendice
            ];
    }

    protected function detallesFactura(){
        $detalles = collect();

        foreach ($this->venta->detalles as $index => $detalle) {

            $cod = Unidad::where('nombre', ucfirst($detalle->unidad))->pluck('cod')->first();
            if ($cod){
                $detalle->cod_medida = $cod;
            }else{
                $detalle->cod_medida = 59;
            }

            // Tipo Item
            if ($detalle->producto()->pluck('tipo')->first() == 'Servicio'){
                $detalle->tipo_item = 2;
            }else{
                $detalle->tipo_item = 1;
            }

            $tributos = NULL;

            if ($detalle->fovial > 0 || $detalle->cotrans > 0) {
                $tributos = [];
                if ($detalle->fovial > 0){ 
                    array_push($tributos, 'D1');
                }
                if ($detalle->cotrans > 0){ 
                    array_push($tributos, 'C8');
                }
            }

            $detalle->codTributo = NULL;

            $detalles->push([
                "numItem" => $index + 1,
                "tipoItem" => $detalle->tipo_item,
                "numeroDocumento" => NULL,
                "cantidad" => floatval(number_format($detalle->cantidad,4, '.', '')),
                "codigo" => $detalle->codigo,
                "codTributo" => $detalle->codTributo,
                "uniMedida" => $detalle->cod_medida,
                "descripcion" => $detalle->producto_nombre,
                "precioUni" => floatval(number_format($detalle->precio,4, '.', '')),
                "montoDescu" => floatval(number_format($detalle->descuento,2, '.', '')),
                "ventaNoSuj" => floatval(number_format($detalle->no_sujeta,2, '.', '')),
                "ventaExenta" => floatval(number_format($detalle->exenta,2, '.', '')),
                "ventaGravada" => floatval(number_format($detalle->gravada + $detalle->iva,2, '.', '')),
                "tributos" => $tributos,
                "psv" => 0,
                "noGravado" => 0,
                "ivaItem" => floatval(number_format($detalle->iva,2))
              ]);
        }

        return $detalles;
    }

    public function generarSujeroExcluido(){
        $tributos = NULL;
        $apendice = NULL;

        if ($this->compra->proveedor->nit) {
            $this->compra->proveedor->tipo_documento = '36';
            $this->compra->proveedor->num_documento = $this->compra->proveedor->nit ? str_replace('-', '', $this->compra->proveedor->nit) : NULL;
        }
        if ($this->compra->proveedor->dui) {
            $this->compra->proveedor->tipo_documento = '13';
            $this->compra->proveedor->num_documento = $this->compra->proveedor->dui ? str_replace('-', '', $this->compra->proveedor->dui) : NULL;
        }

        return 
            [
                "identificacion" => [
                    "version" => $this->compra->version,
                    "ambiente" => $this->compra->ambiente,
                    "tipoDte" => $this->compra->tipo_dte,
                    "numeroControl" => $this->compra->numero_control,
                    "codigoGeneracion" => $this->compra->codigo_generacion,
                    "tipoModelo" => $this->compra->tipoModelo,
                    "tipoOperacion" => $this->compra->tipoOperacion,
                    "tipoContingencia" => $this->compra->tipoContingencia,
                    "motivoContin" => $this->compra->motivoContin,
                    "fecEmi" => \Carbon\Carbon::parse($this->compra->fecha)->format('Y-m-d'),
                    "horEmi" => \Carbon\Carbon::parse($this->compra->created_at)->format('H:i:s'),
                    "tipoMoneda" => $this->compra->moneda,
                ],
                "emisor" => [
                    "nit" => str_replace('-', '', $this->empresa->nit),
                    "nrc" => str_replace('-', '', $this->empresa->nrc),
                    "nombre" => $this->empresa->nombre,
                    "codActividad" => $this->empresa->cod_actividad_economica,
                    "descActividad" => $this->empresa->actividad_economica,
                    "direccion" => [
                        "departamento" => $this->empresa->cod_departamento,
                        "municipio" => $this->empresa->cod_municipio,
                        "complemento" => $this->empresa->direccion,
                    ],
                    "telefono" => $this->empresa->telefono,
                    "codEstableMH" => $this->empresa->cod_estable_mh ? $this->empresa->cod_estable_mh : NULL,
                    "codEstable" => $this->empresa->cod_estable ? $this->empresa->cod_estable : NULL,
                    "codPuntoVentaMH" => NULL,
                    "codPuntoVenta" => NULL,
                    "correo" => $this->empresa->correo,
                ],
                "sujetoExcluido" =>  [
                    "tipoDocumento" => $this->compra->proveedor->tipo_documento, //36 NIT 13 DUI
                    "numDocumento" => $this->compra->proveedor->num_documento,
                    "nombre" => $this->compra->proveedor_nombre,
                    "codActividad" => $this->compra->proveedor->cod_giro ? $this->compra->proveedor->cod_giro : NULL,
                    "descActividad" => $this->compra->proveedor->giro ? $this->compra->proveedor->giro : NULL,
                    "direccion" => [
                        "departamento" => $this->compra->proveedor->cod_departamento,
                        "municipio" => $this->compra->proveedor->cod_municipio,
                        "complemento" => $this->compra->proveedor->direccion
                    ],
                    "telefono" => $this->compra->proveedor->telefono,
                    "correo" => $this->compra->proveedor->correo
                ],
                "cuerpoDocumento" => $this->detallesSujetoExcluido(),
                "resumen" => [
                  "totalCompra" => floatval(number_format($this->compra->subtotal, 2, '.', '')),
                  "descu" => floatval(number_format($this->compra->descuento, 2, '.', '')),
                  "totalDescu" => floatval(number_format($this->compra->descuento, 2, '.', '')),
                  "subTotal" => floatval(number_format($this->compra->subtotal, 2, '.', '')),
                  "ivaRete1" => floatval(number_format($this->compra->iva_retenido, 2, '.', '')),
                  "reteRenta" => floatval(number_format($this->compra->renta_retenida, 2, '.', '')),
                  "totalPagar" => floatval(number_format($this->compra->total, 2, '.', '')),
                  "totalLetras" => $this->compra->total_en_letras,
                  "condicionOperacion" => $this->compra->cod_condicion,
                  "pagos" => [
                        [
                          "codigo" => $this->compra->cod_metodo_pago,
                          "montoPago" => floatval(number_format($this->compra->total, 2, '.', '')),
                          "referencia" => NULL,
                          "plazo" => NULL,
                          "periodo" => NULL
                        ]
                    ],
                  "observaciones" => null
                ],
                "apendice" => $apendice
            ];
    }

    protected function detallesSujetoExcluido(){
        $detalles = collect();

        foreach ($this->compra->detalles as $index => $detalle) {

            //Unidad
            $detalle->cod_medida = 59;
            // Tipo Item
            $detalle->tipo_item = 2; //Servicio
            // $detalle->tipo_item = 1; //Producto

            $detalles->push([
                "numItem" => $index + 1,
                "tipoItem" => $detalle->tipo_item,
                "cantidad" => floatval(number_format($detalle->cantidad,4, '.', '')),
                "codigo" => $detalle->codigo,
                "uniMedida" => $detalle->cod_medida,
                "descripcion" => $detalle->producto_nombre,
                "precioUni" => floatval(number_format($detalle->costo ,4, '.', '')),
                "montoDescu" => floatval(number_format($detalle->descuento,2, '.', '')),
                "compra" => floatval(number_format($detalle->total,2, '.', '')),
              ]);
        }

        return $detalles;
    }

    public function generarCCF(){

        $tributos = NULL;

        $apendice = NULL;
        
        if ($this->venta->placa || $this->venta->kilometraje || $this->venta->observacion ) {
            $apendice = collect();
            if ($this->venta->placa) {
                $apendice->push(['etiqueta' => 'Placa', 'campo' => 'Placa', 'valor'=> $this->venta->placa]);
            }
            if ($this->venta->kilometraje) {
                $apendice->push(['etiqueta' => 'Kilometraje', 'campo' => 'Kilometraje', 'valor'=> $this->venta->kilometraje]);
            }
            if ($this->venta->observacion) {
                $apendice->push(['etiqueta' => 'Observaciones', 'campo' => 'Observaciones', 'valor'=> $this->venta->observacion]);
            }
        }

        // if ($this->venta->iva) {
            $tributos = collect();
            if ($this->venta->iva){ 
                $tributos->push(['codigo' => '20', 'descripcion'=> 'Impuesto al Valor Agregado 13%', 'valor' => floatval(number_format($this->venta->iva,2, '.', ''))]);
            }
            if ($this->venta->fovial > 0){ 
                $tributos->push(['codigo' => 'D1', 'descripcion'=> 'FOVIAL ($0.20 Ctvs. por galón)', 'valor' => floatval(number_format($this->venta->fovial,2, '.', ''))]);
            }
            if ($this->venta->cotrans > 0){ 
                $tributos->push(['codigo' => 'C8', 'descripcion'=> 'COTRANS ($0.10 Ctvs. por galón)', 'valor' => floatval(number_format($this->venta->cotrans,2, '.', ''))]);
            }
        // }

        return 
            [
                "identificacion" => $this->identificador(),
                "documentoRelacionado" => NULL,
                "emisor" => $this->emisor(),
                "receptor" => $this->receptor(),
                "otrosDocumentos" => NULL,
                "ventaTercero" => NULL,
                "cuerpoDocumento" => $this->detallesCCF(),
                "resumen" => [
                  "totalNoSuj" => floatval(number_format($this->venta->no_sujeta, 2, '.', '')),
                  "totalExenta" => floatval(number_format($this->venta->exenta, 2, '.', '')),
                  "totalGravada" => floatval(number_format($this->venta->gravada, 2, '.', '')),
                  "subTotalVentas" => floatval(number_format($this->venta->subtotal, 2, '.', '')),
                  "descuNoSuj" => 0,
                  "descuExenta" => 0,
                  "descuGravada" => floatval(number_format($this->venta->descuento, 2, '.', '')),
                  "porcentajeDescuento" => 0,
                  "totalDescu" => floatval(number_format($this->venta->descuento, 2, '.', '')),
                  "tributos" => $tributos,
                  "subTotal" => floatval(number_format($this->venta->subtotal, 2, '.', '')),
                  "ivaPerci1" => floatval(number_format($this->venta->iva_percibido, 2, '.', '')),
                  "ivaRete1" => floatval(number_format($this->venta->iva_retenido, 2, '.', '')),
                  "reteRenta" => 0,
                  "montoTotalOperacion" => floatval(number_format($this->venta->total, 2, '.', '')),
                  "totalNoGravado" => 0,
                  "totalPagar" => floatval(number_format($this->venta->total, 2, '.', '')),
                  "totalLetras" => $this->venta->total_en_letras,
                  // "totalIva" => floatval(number_format($this->venta->iva, 2, '.', '')),
                  "saldoFavor" => 0,
                  "condicionOperacion" => $this->venta->cod_condicion,
                  "pagos" => [
                    [
                      "codigo" => $this->venta->cod_metodo_pago,
                      "montoPago" => floatval(number_format($this->venta->total, 2, '.', '')),
                      "referencia" => NULL,
                      "plazo" => NULL,
                      "periodo" => NULL
                    ]
                  ],
                  "numPagoElectronico" => ""
                ],
                "extension" => NULL,
                "apendice" => $apendice
            ];
    }

    protected function detallesCCF(){
        $detalles = collect();

        foreach ($this->venta->detalles as $index => $detalle) {

            $cod = Unidad::where('nombre', ucfirst($detalle->unidad))->pluck('cod')->first();
            if ($cod){
                $detalle->cod_medida = $cod;
            }else{
                $detalle->cod_medida = 59;
            }

            // Tipo Item
            if ($detalle->producto()->pluck('tipo')->first() == 'Servicio'){
                $detalle->tipo_item = 2;
            }else{
                $detalle->tipo_item = 1;
            }

            $tributos = NULL;

            // if ($detalle->iva) {
                $tributos = [];
                if ($detalle->iva){ 
                    array_push($tributos, '20');
                }
                if ($detalle->fovial > 0){ 
                    array_push($tributos, 'D1');
                }
                if ($detalle->cotrans > 0){ 
                    array_push($tributos, 'C8');
                }
            // }

            $detalle->codTributo = NULL;

            if ($detalle->iva > 0) {
                $detalle->precio  =  $detalle->precio / 1.13;
            }

            $detalles->push([
                "numItem" => $index + 1,
                "tipoItem" => $detalle->tipo_item,
                "numeroDocumento" => NULL,
                "cantidad" => floatval(number_format($detalle->cantidad,4, '.', '')),
                "codigo" => $detalle->codigo,
                "codTributo" => $detalle->codTributo,
                "uniMedida" => $detalle->cod_medida,
                "descripcion" => $detalle->producto_nombre,
                "precioUni" => floatval(number_format($detalle->precio,4, '.', '')),
                "montoDescu" => floatval(number_format($detalle->descuento,2, '.', '')),
                "ventaNoSuj" => floatval(number_format($detalle->no_sujeta,2, '.', '')),
                "ventaExenta" => floatval(number_format($detalle->exenta,2, '.', '')),
                "ventaGravada" => floatval(number_format($detalle->gravada,2, '.', '')),
                "tributos" => $tributos,
                "psv" => 0,
                "noGravado" => 0,
                // "ivaItem" => floatval($detalle->iva)
              ]);
        }

        return $detalles;
    }


}

