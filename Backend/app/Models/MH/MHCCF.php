<?php

namespace App\Models\MH;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Http;
use App\Models\MH\Unidad;
use Luecano\NumeroALetras\NumeroALetras;

class MHCCF extends Model
{

    public $venta;
    public $caja;
    public $caja_codigo;
    public $empresa;
    

    public function generarDTE($venta){
        $this->venta = $venta;
        $this->empresa = $this->venta->empresa()->first();
        $this->caja = $this->venta->caja()->first();
        $this->sucursal = $this->venta->sucursal()->first();

        $this->caja_codigo = $this->caja->codigo;
        $this->venta->tipo_dte = '03';
        $this->venta->numero_control = 'DTE-'. $this->venta->tipo_dte . '-' . $this->sucursal->cod_estable_mh . $this->caja_codigo . '-' .str_pad($this->venta->correlativo, 15, '0', STR_PAD_LEFT);

        if (!$this->venta->codigo_generacion) {
            $this->venta->codigo_generacion = strtoupper(Uuid::uuid4()->toString());
        }
        $this->venta->save();

        $this->venta->ambiente = $this->empresa->fe_ambiente; // 00 Modo prueba 01 Modo producción
        $this->venta->tipoModelo = 1; // 1 Modelo Facturación previo 2 Modelo Facturación diferido
        $this->venta->tipoOperacion = 1; // 1 Transmisión normal 2 Transmisión por contingencia
        $this->venta->tipoContingencia = NULL; // 1 No disponibilidad de sistema del MH 2 No disponibilidad de sistema del emisor 3 Falla en el suministro de servicio de Internet del Emisor 4 Falla en el suministro de servicio de energía eléctrica del emisor que impida la transmisión de los DTE 5 Otro (deberá digitar un máximo de 500 caracteres explicando el motivo)
        $this->venta->motivoContin = NULL;
        $this->venta->moneda = 'USD';
        $this->venta->version = 3;


        // Condición
            if ($this->venta->condicion == 'Crédito'){
                $this->venta->cod_condicion = 2; //Credito
            }else{
                $this->venta->cod_condicion = 1; //Contado
            }

        // Metodo de pago
            switch ($this->venta->metodo_pago) {
                case 'Efectivo': //Billetes y monedas
                    $this->venta->cod_metodo_pago = '01';
                    break;
                case 'Tarjeta': //Tarjeta Débito y Credito
                    $this->venta->cod_metodo_pago = '02';
                    break;
                case 'Cheque': //Tarjeta Débito
                    $this->venta->cod_metodo_pago = '04';
                    break;
                case 'Transferencia': //Transferencia_ Depósito Bancario
                    $this->venta->cod_metodo_pago = '05';
                    break;
                case 'Vales': //Vales o Cupones
                    $this->venta->cod_metodo_pago = '01';
                    break;
                case 'Chivo Wallet': //Dinero electrónico
                    $this->venta->cod_metodo_pago = '09';
                    break;
                case 'Bitcoin': //Dinero electrónico
                    $this->venta->cod_metodo_pago = '11';
                    break;
                default:
                    $this->venta->cod_metodo_pago = '01';
                    break;
            }

        // Total en letras
        $partes = explode('.', strval( number_format($this->venta->total, 2, '.', '') ));
        $this->venta->total_en_letras = \NumeroALetras::convertir($partes[0], 'DOLARES CON ') . $partes[1].'/100';
        
        return $this->generarCCF();

    } 

    protected function identificador(){
        return [
            "version" => $this->venta->version,
            "ambiente" => $this->venta->ambiente,
            "tipoDte" => $this->venta->tipo_dte,
            "numeroControl" => $this->venta->numero_control,
            "codigoGeneracion" => $this->venta->codigo_generacion,
            "tipoModelo" => $this->venta->tipoModelo,
            "tipoOperacion" => $this->venta->tipoOperacion,
            "tipoContingencia" => $this->venta->tipoContingencia,
            "motivoContin" => $this->venta->motivoContin,
            "fecEmi" => \Carbon\Carbon::parse($this->venta->fecha)->format('Y-m-d'),
            "horEmi" => \Carbon\Carbon::parse($this->venta->created_at)->format('H:i:s'),
            "tipoMoneda" => $this->venta->moneda,
        ];
    }

    protected function emisor(){
        
        return [
            "nit" => str_replace('-', '', $this->empresa->nit),
            "nrc" => str_replace('-', '', $this->empresa->nrc),
            "nombre" => $this->empresa->nombre,
            "codActividad" => $this->empresa->cod_actividad_economica,
            "descActividad" => $this->empresa->actividad_economica,
            "nombreComercial" => $this->empresa->nombre_comercial,
            "tipoEstablecimiento" => $this->empresa->tipo_establecimiento,
            "direccion" => [
                "departamento" => $this->empresa->cod_departamento,
                "municipio" => $this->empresa->cod_municipio,
                "complemento" => $this->empresa->direccion,
            ],
            "telefono" => $this->empresa->telefono,
            "codEstableMH" => $this->empresa->cod_estable_mh ? $this->empresa->cod_estable_mh : NULL,
            "codEstable" => $this->empresa->cod_estable ? $this->empresa->cod_estable : NULL,
            "codPuntoVentaMH" => $this->caja->codigo ? $this->caja->codigo : NULL,
            "codPuntoVenta" => $this->caja->codigo ? $this->caja->codigo : NULL,
            "correo" => $this->empresa->correo,
        ];
    }

    protected function receptor(){

        return [
              "nit" =>  $this->venta->cliente->nit ? str_replace('-', '', $this->venta->cliente->nit) : str_replace('-', '', $this->venta->cliente->dui),
              "nombreComercial" =>  $this->venta->cliente->nombre_empresa,
              "nrc" => str_replace('-', '', $this->venta->cliente->registro),
              "nombre" => $this->venta->cliente_nombre,
              "codActividad" => $this->venta->cliente->cod_giro,
              "descActividad" => $this->venta->cliente->giro,
              "direccion" => [
                "departamento" => $this->venta->cliente->cod_departamento,
                "municipio" => $this->venta->cliente->cod_departamento,
                "complemento" => $this->venta->cliente->direccion
              ],
              "telefono" => $this->venta->cliente->telefono,
              "correo" => $this->venta->cliente->correo
            ];
    }

    public function generarCCF(){

        $tributos = NULL;

        $apendice = NULL;
        
        if ($this->venta->placa || $this->venta->kilometraje || $this->venta->observacion ) {
            $apendice = collect();
            if ($this->venta->placa) {
                $apendice->push(['etiqueta' => 'Placa', 'campo' => 'Placa', 'valor'=> $this->venta->placa]);
            }
            if ($this->venta->kilometraje) {
                $apendice->push(['etiqueta' => 'Kilometraje', 'campo' => 'Kilometraje', 'valor'=> $this->venta->kilometraje]);
            }
            if ($this->venta->observacion) {
                $apendice->push(['etiqueta' => 'Observaciones', 'campo' => 'Observaciones', 'valor'=> $this->venta->observacion]);
            }
        }

        // if ($this->venta->iva) {
            $tributos = collect();
            if ($this->venta->iva){ 
                $tributos->push(['codigo' => '20', 'descripcion'=> 'Impuesto al Valor Agregado 13%', 'valor' => floatval(number_format($this->venta->iva,2, '.', ''))]);
            }
            if ($this->venta->fovial > 0){ 
                $tributos->push(['codigo' => 'D1', 'descripcion'=> 'FOVIAL ($0.20 Ctvs. por galón)', 'valor' => floatval(number_format($this->venta->fovial,2, '.', ''))]);
            }
            if ($this->venta->cotrans > 0){ 
                $tributos->push(['codigo' => 'C8', 'descripcion'=> 'COTRANS ($0.10 Ctvs. por galón)', 'valor' => floatval(number_format($this->venta->cotrans,2, '.', ''))]);
            }
        // }

        return 
            [
                "identificacion" => $this->identificador(),
                "documentoRelacionado" => NULL,
                "emisor" => $this->emisor(),
                "receptor" => $this->receptor(),
                "otrosDocumentos" => NULL,
                "ventaTercero" => NULL,
                "cuerpoDocumento" => $this->detalles(),
                "resumen" => [
                  "totalNoSuj" => floatval(number_format($this->venta->no_sujeta, 2, '.', '')),
                  "totalExenta" => floatval(number_format($this->venta->exenta, 2, '.', '')),
                  "totalGravada" => floatval(number_format($this->venta->gravada, 2, '.', '')),
                  "subTotalVentas" => floatval(number_format($this->venta->subtotal, 2, '.', '')),
                  "descuNoSuj" => 0,
                  "descuExenta" => 0,
                  "descuGravada" => floatval(number_format($this->venta->descuento, 2, '.', '')),
                  "porcentajeDescuento" => 0,
                  "totalDescu" => floatval(number_format($this->venta->descuento, 2, '.', '')),
                  "tributos" => $tributos,
                  "subTotal" => floatval(number_format($this->venta->subtotal, 2, '.', '')),
                  "ivaPerci1" => floatval(number_format($this->venta->iva_percibido, 2, '.', '')),
                  "ivaRete1" => floatval(number_format($this->venta->iva_retenido, 2, '.', '')),
                  "reteRenta" => 0,
                  "montoTotalOperacion" => floatval(number_format($this->venta->total, 2, '.', '')),
                  "totalNoGravado" => 0,
                  "totalPagar" => floatval(number_format($this->venta->total, 2, '.', '')),
                  "totalLetras" => $this->venta->total_en_letras,
                  // "totalIva" => floatval(number_format($this->venta->iva, 2, '.', '')),
                  "saldoFavor" => 0,
                  "condicionOperacion" => $this->venta->cod_condicion,
                  "pagos" => [
                    [
                      "codigo" => $this->venta->cod_metodo_pago,
                      "montoPago" => floatval(number_format($this->venta->total, 2, '.', '')),
                      "referencia" => NULL,
                      "plazo" => NULL,
                      "periodo" => NULL
                    ]
                  ],
                  "numPagoElectronico" => ""
                ],
                "extension" => NULL,
                "apendice" => $apendice
            ];
    }

    protected function detalles(){
        $detalles = collect();

        foreach ($this->venta->detalles as $index => $detalle) {

            $cod = Unidad::where('nombre', ucfirst($detalle->unidad))->pluck('cod')->first();
            if ($cod){
                $detalle->cod_medida = $cod;
            }else{
                $detalle->cod_medida = 59;
            }

            // Tipo Item
            if ($detalle->producto()->pluck('tipo')->first() == 'Servicio'){
                $detalle->tipo_item = 2;
            }else{
                $detalle->tipo_item = 1;
            }

            $tributos = NULL;

            // if ($detalle->iva) {
                $tributos = [];
                if ($detalle->iva){ 
                    array_push($tributos, '20');
                }
                if ($detalle->fovial > 0){ 
                    array_push($tributos, 'D1');
                }
                if ($detalle->cotrans > 0){ 
                    array_push($tributos, 'C8');
                }
            // }

            $detalle->codTributo = NULL;

            if ($detalle->iva > 0) {
                $detalle->precio  =  $detalle->precio / 1.13;
            }

            $detalles->push([
                "numItem" => $index + 1,
                "tipoItem" => $detalle->tipo_item,
                "numeroDocumento" => NULL,
                "cantidad" => floatval(number_format($detalle->cantidad,4, '.', '')),
                "codigo" => $detalle->codigo,
                "codTributo" => $detalle->codTributo,
                "uniMedida" => $detalle->cod_medida,
                "descripcion" => $detalle->descripcion,
                "precioUni" => floatval(number_format($detalle->precio,4, '.', '')),
                "montoDescu" => floatval(number_format($detalle->descuento,2, '.', '')),
                "ventaNoSuj" => floatval(number_format($detalle->no_sujeta,2, '.', '')),
                "ventaExenta" => floatval(number_format($detalle->exenta,2, '.', '')),
                "ventaGravada" => floatval(number_format($detalle->gravada,2, '.', '')),
                "tributos" => $tributos,
                "psv" => 0,
                "noGravado" => 0,
                // "ivaItem" => floatval($detalle->iva)
              ]);
        }

        return $detalles;
    }


}

