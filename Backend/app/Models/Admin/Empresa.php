<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empresa extends Model {

    use SoftDeletes;
    protected $table = 'empresas';
    protected $fillable = [
        'nombre',
        'direccion',
        'telefono',
        'correo',
        'municipio',
        'cod_municipio',
        'departamento',
        'cod_departamento',
        'distrito',
        'cod_distrito',
        'logo',
        'meta_galones',
        'valor_inventario',
        'bodega_compra_id',
        'nit',
        'nrc',
        'nombre_propietario',
        'nombre_comercial',
        'actividad_economica',
        'cod_actividad_economica',
        'tipo_establecimiento',
        'mh_pwd_certificado',
        'mh_usuario',
        'mh_contrasena',
        'cod_estable_mh',
        'cod_estable',
        'facturacion_electronica',
        'fe_ambiente',
        'enviar_dte',
        'imprimir_dte',
        'vender_sin_stock',
        'editar_precio_venta',
        'ips'
    ];

    public function getIpsAttribute($value) 
    {
        return is_string($value) ? json_decode($value) : $value;
    }


}
