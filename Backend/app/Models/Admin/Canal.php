<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Canal extends Model {

    protected $table = 'empresa_canales';
    protected $fillable = array(
        'nombre',
        'empresa_id'
    );

    public function empresa(){
        return $this->belongsTo('App\Models\Admin\Empresa','empresa_id');
    }

    public function ventas(){
        return $this->hasMany('App\Models\Ventas\Venta','canal_id');
    }



}
