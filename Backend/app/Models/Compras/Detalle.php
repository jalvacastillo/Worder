<?php

namespace App\Models\Compras;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model {

    protected $table = 'compra_detalles';
    protected $fillable = array(
        'producto_id',
        'descripcion',
        'cantidad',
        'costo',
        'descuento',
        'no_sujeta',
        'exenta',
        'gravada',
        'iva',
        'subtotal',
        'total',
        'compra_id'

    );

    protected $appends = ['nombre_producto', 'medida'];

    public function getNombreProductoAttribute(){
        return $this->producto()->pluck('nombre')->first();
    }

    public function getMedidaAttribute(){
        return $this->producto()->pluck('medida')->first();
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto','producto_id');
    }

    public function compra(){
        return $this->belongsTo('App\Models\Compras\Compra','compra_id');
    }


}
