<?php 

use App\Http\Controllers\Api\Admin\MHController;

    // Route::get('/municipios', [MHController::class, 'municipios']);
    // Route::get('/departamentos', [MHController::class, 'departamentos']);
    // Route::get('/actividades_economicas', [MHController::class, 'actividadesEconomicas']);
    // Route::get('/unidades', [MHController::class, 'unidades']);

    // Route::get('/reporte/ticket/{id}',  [MHController::class, 'generarTicket']);
    // Route::post('/emitirDTE',           [MHController::class, 'emitirDTE']);
    // Route::post('/generarDTE',          [MHController::class, 'generarDTE']);
    // Route::post('/generarDTESujetoExcluido',          [MHController::class, 'generarDTESujetoExcluido']);
    // Route::post('/enviarDTE',           [MHController::class, 'enviarDTE']);
    // Route::post('/enviarDTE/sujetoexcluido',           [MHController::class, 'enviarDTESujetoExcluido']);
    // Route::post('/anularDTE',           [MHController::class, 'anularDTE']);
    // Route::post('/anularDTE/sujetoexcluido',           [MHController::class, 'anularDTESujetoExcluido']);
    // Route::get('/reporte/dte/{id}',     [MHController::class, 'generarDTEPDF']);
    // Route::get('/reporte/dte/sujetoexcluido/{id}',     [MHController::class, 'generarDTEPDFSujetoExcluido']);
    // Route::get('/reporte/dte-json/{id}',    [MHController::class, 'generarDTEJSON']);
    // Route::get('/reporte/dte-json/sujetoexcluido/{id}',    [MHController::class, 'generarDTEJSONSujetoExcluido']);

    // Route::post('/generarDTEAnulado',    [MHController::class, 'generarDTEAnulado']);
    // Route::post('/generarDTEAnuladoSujetoExcluido',    [MHController::class, 'generarDTEAnuladoSujetoExcluido']);

    // Route::post('/generarContingencia',    [MHController::class, 'generarContingencia']);

use App\Http\Controllers\Api\Admin\MHDTEController;

    Route::get('/paises', [MHController::class, 'paises']);
    Route::get('/municipios', [MHController::class, 'municipios']);
    Route::get('/distritos', [MHController::class, 'distritos']);
    Route::get('/departamentos', [MHController::class, 'departamentos']);
    Route::get('/actividades_economicas', [MHController::class, 'actividadesEconomicas']);
    Route::get('/unidades', [MHController::class, 'unidades']);

    // Generar DTE
    Route::post('/generarDTE',          [MHDTEController::class, 'generarDTE']);
    Route::post('/generarDTENotaCredito',          [MHDTEController::class, 'generarDTENotaCredito']);
    Route::post('/generarDTESujetoExcluidoGasto', [MHDTEController::class, 'generarDTESujetoExcluidoGasto']);
    Route::post('/generarDTESujetoExcluidoCompra', [MHDTEController::class, 'generarDTESujetoExcluidoCompra']);
    Route::post('/generarDTEAnuladoSujetoExcluidoGasto',    [MHDTEController::class, 'generarDTEAnuladoSujetoExcluidoGasto']);
    Route::post('/generarDTEAnuladoSujetoExcluidoCompra',    [MHDTEController::class, 'generarDTEAnuladoSujetoExcluidoCompra']);
    Route::post('/generarContingencia',    [MHDTEController::class, 'generarContingencia']);
    Route::post('/generarDTEAnulado',    [MHDTEController::class, 'generarDTEAnulado']);
    Route::post('/consultarDTE',    [MHDTEController::class, 'consultarDTE']);

    // Generar facturas
    Route::get('/reporte/ticket/{id}',  [MHDTEController::class, 'generarTicket']);

    // Enviar DTE
    Route::post('/enviarDTE',           [MHDTEController::class, 'enviarDTE']);
    // Anular DTE
    Route::post('/anularDTE',           [MHDTEController::class, 'anularDTE']);
    // Generar DTE JSON
    Route::get('/reporte/dte/{id}/{tipo}',     [MHDTEController::class, 'generarDTEPDF']);
    // Generar DTE PDF
    Route::get('/reporte/dte-json/{id}/{tipo}',    [MHDTEController::class, 'generarDTEJSON']);

?>
