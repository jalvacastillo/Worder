<div class="section">
    <div class="container">
        
    <div class="row justify-content-center px-0 px-lg-5 mx-0 mx-lg-5">
        <div class="col-md-12 mb-5 text-center">
            <h2 class="title">¿Qué incluye?</h2>
            <h4>Wanda es un sistema integrado por 8 módulos:</h4>
        </div>
    </div>
    
    <div class="row mb-4 mb-md-0">
        <div class="col-md-6 order-2 order-md-1">
            <img class="d-block w-100" src="{{ asset('/img/galeria/dash.jpg') }}" alt="First slide">
        </div>
        <div class="col-md-6 order-1 order-md-2">
            <div class="text-center">
                <i class="large material-icons text-primary">dashboard</i>
                <h3>Dashboad</h3>
            </div>
            <p>Gestiona de mejor manera tu negocio monitoreando:</p>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Indicadores de ventas y compras</li>
                <li><i class="fa fa-check text-info"></i> Productos más vendidos</li>
                <li><i class="fa fa-check text-info"></i> Datos sobre ordenes</li>
                <li><i class="fa fa-check text-info"></i> Estadisticas de vendedores</li>
            </ul>
        </div>
    </div>
    <img class="d-none d-md-block m-auto" height="150px" src="{{ asset('img/flecha2.png') }}" alt="flecha">
    <div class="row">
        <div class="col-md-6 order-2">
            <img class="d-block w-100" src="{{ asset('/img/galeria/administrador.jpg') }}" alt="First slide">
        </div>
        <div class="col-md-6 order-1">
            <div class="text-center">
                <i class="large material-icons text-gray">build</i>
                <h3>Administrador</h3>
            </div>
            <p>Permite administrar las partes esenciales del negocio:</p>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Control de sucursales</li>
                <li><i class="fa fa-check text-info"></i> Control de bodegas</li>
                <li><i class="fa fa-check text-info"></i> Gestión de cajas</li>
                <li><i class="fa fa-check text-info"></i> Configuraciones generales</li>
            </ul>
        </div>
    </div>
    <img class="d-none d-md-block m-auto" height="150px" src="{{ asset('img/flecha1.png') }}" alt="flecha">
    <div class="row mb-4 mb-md-0">
        <div class="col-md-6 order-2 order-md-1">
            <img class="d-block w-100" src="{{ asset('/img/galeria/ventas.jpg') }}" alt="First slide">
        </div>
        <div class="col-md-6 order-1 order-md-2">
            <div class="text-center">
                <i class="large material-icons text-success">receipt</i>
                <h3>Facturación</h3>
                <p>Facilita el control de:</p>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Gestión de ventas y créditos</li>
                <li><i class="fa fa-check text-info"></i> Facturación e impresión de ticket o documento</li>
                <li><i class="fa fa-check text-info"></i> Gestión de clientes y cuentas por cobrar</li>
                <li><i class="fa fa-check text-info"></i> Historial de ventas por fecha, por producto y por categorías</li>
            </ul>
        </div>
    </div>
    <img class="d-none d-md-block m-auto" height="150px" src="{{ asset('img/flecha2.png') }}" alt="flecha">
    <div class="row mb-4 mb-md-0">
        <div class="col-md-6 order-2">
            <img class="d-block w-100" src="{{ asset('/img/galeria/compras.jpg') }}" alt="First slide">
        </div>
        <div class="col-md-6 order-1">
            <div class="text-center">
                <i class="large material-icons text-warning">shopping_cart</i>
                <h3>Compras</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Registro de facturas</li>
                <li><i class="fa fa-check text-info"></i> Cargo a inventario</li>
                <li><i class="fa fa-check text-info"></i> Gestión de proveedores y cuentas por pagar</li>
                <li><i class="fa fa-check text-info"></i> Historial de compras por fecha, por producto y por categorías</li>
            </ul>
        </div>
    </div>
    <img class="d-none d-md-block m-auto" height="150px" src="{{ asset('img/flecha1.png') }}" alt="flecha">
    <div class="row mb-4 mb-md-0">
        <div class="col-md-6 order-2 order-md-1">
            <img class="d-block w-100" src="{{ asset('/img/galeria/gastos.jpg') }}" alt="First slide">
        </div>
        <div class="col-md-6 order-1 order-md-2">
            <div class="text-center">
                <i class="large material-icons text-secondary">account_balance_wallet</i>
                <h3>Contabilidad</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Caja chica</li>
                <li><i class="fa fa-check text-info"></i> Control de gastos</li>
                <li><i class="fa fa-check text-info"></i> Libro de IVA</li>
                <li><i class="fa fa-check text-info"></i> Libro de compras</li>
            </ul>
        </div>
    </div>
    <img class="d-none d-md-block m-auto" height="150px" src="{{ asset('img/flecha2.png') }}" alt="flecha">
    <div class="row mb-4 mb-md-0">
        <div class="col-md-6 order-2">
            <img class="d-block w-100" src="{{ asset('/img/galeria/inventario.jpg') }}" alt="First slide">
        </div>
        <div class="col-md-6 order-1">
            <div class="text-center">
                <i class="large material-icons text-brown">inbox</i>
                <h3>Inventario</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Gestión de productos</li>
                <li><i class="fa fa-check text-info"></i> Gestión de materias primas</li>
                <li><i class="fa fa-check text-info"></i> Kardex y control de stock</li>
                <li><i class="fa fa-check text-info"></i> Promociones y combos</li>
                <li><i class="fa fa-check text-info"></i> Ajustes y traslados</li>
            </ul>
        </div>
    </div>

    <img class="d-none d-md-block m-auto" height="150px" src="{{ asset('img/flecha1.png') }}" alt="flecha">
    <div class="row mb-4 mb-md-0">
        <div class="col-md-6 order-2 order-md-1">
            <img class="d-block w-100" src="{{ asset('/img/galeria/empleados.jpg') }}" alt="First slide">
        </div>
        <div class="col-md-6 order-1 order-md-2">
            <div class="text-center">
                <i class="large material-icons text-secondary">account_balance_wallet</i>
                <h3>Empleados</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Gestión de empleados</li>
                <li><i class="fa fa-check text-info"></i> Planilla</li>
                <li><i class="fa fa-check text-info"></i> Asistencia</li>
                <li><i class="fa fa-check text-info"></i> Planilla</li>
            </ul>
        </div>
    </div>

    <img class="d-none d-md-block m-auto" height="150px" src="{{ asset('img/flecha2.png') }}" alt="flecha">
    <div class="row mb-4 mb-md-0">
        <div class="col-md-6 order-2">
            <img class="d-block w-100" src="{{ asset('/img/galeria/creditos.jpg') }}" alt="First slide">
        </div>
        <div class="col-md-6 order-1">
            <div class="text-center">
                <i class="large material-icons text-brown">inbox</i>
                <h3>Créditos</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Gestión de créditos</li>
                <li><i class="fa fa-check text-info"></i> Gestión de pagos de clientes</li>
                <li><i class="fa fa-check text-info"></i> Generación de tables de amortización</li>
            </ul>
        </div>
    </div>

    </div>
</div>
