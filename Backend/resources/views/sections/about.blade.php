<div class="section text-center pb-0">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto">
        <h2 itemprop="name" class="title">
            ¿Qué es {{ env('APP_NAME') }}?
        </h2>
        <h5 itemprop="description" class="description">
            {{ env('APP_NAME') }} te permite gestionar tu empresa sin importar donde estés.
            <br> <br>
            Monitorea y automatiza los procesos de facturación, la gestión de inventarios, compras, clientes, proveedores, empleados y más.
        </h5>
      </div>
    </div>
    </div>
</div>
