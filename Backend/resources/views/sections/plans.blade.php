<div class="planes section section-full-screen section-signup header-filter pb-2" style="background-image: url('img/bg1.jpeg'); background-size: cover; background-position: top center; min-height: 820px;">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 text-center text-white">
			    <h2>Precios</h2>
			</div>
			<div class="col-sm-6">
				<div class="card card-info">
					<div class="card-heading text-center">
						<h2 class="mb-0">Pago mensual</h2>
						<h3 class="font-weight-bold mt-0">
							<small style="text-decoration: line-through;">$59</small> $39
						</h3>
					</div>
					<hr class="my-0">
					<div class="card-body pl-3 pl-lg-5">
						<p><i class="fa fa-check-square text-warning mx-3"></i> Sistema con 8 módulos de gestión</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Servidor en línea 24/7</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Instalación y configuración</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Llenado de datos iniciales</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Capacitación de personal</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Soporte</p>
					</div>
					<hr class="my-0">
					<div class="col-12 text-center">
						<a href="{{ route('registro') }}" class="my-3 btn btn-primary btn-lg color-filter">
							Pruébalo gratis
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card card-info">
					<div class="card-heading text-center">
						<h2 class="mb-0">Pago anual</h2>
						<h3 class="font-weight-bold mt-0">
							<small style="text-decoration: line-through;">$499</small> $399
						</h3>
					</div>
					<hr class="my-0">
					<div class="card-body pl-3 pl-lg-5">
						<p><i class="fa fa-check-square text-warning mx-3"></i> Sistema con 8 módulos de gestión</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Servidor en línea 24/7</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Instalación y configuración</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Llenado de datos iniciales</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Capacitación de personal</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Soporte</p>
					</div>
					<hr class="my-0">
					<div class="col-12 text-center">
						<a href="{{ route('registro') }}" class="my-3 btn btn-primary btn-lg color-filter">
							Pruébalo gratis
						</a>
					</div>
				</div>
			</div>
{{-- 			<div class="col-sm-6">
				<div class="card card-info">
					<div class="card-heading text-center">
						<h2 class="mb-0">Todo Incluido</h2>
						<h3 class="font-weight-bold mt-0">
							<small style="text-decoration: line-through;">$2,799</small> $2,399
						</h3>
					</div>
					<hr class="my-0">
					<div class="card-body pl-3 pl-lg-5">
						<p><i class="fa fa-check-square text-warning mx-3"></i> Sistema con 8 módulos de gestión</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Instalación y configuración</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Llenado de datos iniciales</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Capacitación de personal</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> Soporte gratis por un mes</p>
						<p><i class="fa fa-check-square text-warning mx-3"></i> 1 computadora, caja registradora, impresora de ticket y lector de códigos de barra</p>
					</div>
					<hr class="my-0">
					<div class="col-12 text-center">
						<a href="{{ route('registro') }}" class="my-3 btn btn-primary btn-lg color-filter">
							Pruébalo gratis
						</a>
					</div>
				</div>
			</div> --}}

			<div class="col-12 text-center text-white">
				<h4>Los precios no incluye IVA.</h4>
				<h4>El sistema se puede adaptar y personalizar a tus necesidades.</h4>
			</div>

		</div>
	</div>
</div>