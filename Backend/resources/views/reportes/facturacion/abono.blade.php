<!DOCTYPE html>
<html>
<head>
    <title>Recibo de Abono</title>
    <style>
        @page { 
            margin: 2cm 2.5cm;
        }
        *{ font-family: sans-serif; color: #333; }
        .text-left { text-align: left; }
        .text-center { text-align: center; }
        .text-right { text-align: right; }
        .header { text-align: center; }
        .header h1 {font-size: 25px; margin:0px}
/*        .header p { position: absolute; top: 15px; left: 100px;}*/
        .footer{ position: fixed; bottom: -10px; opacity: .6; }
        .footer span {position: absolute; bottom: 0px; right: 0px; counter-increment: page;}

        .details label {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <div class="header">
        <img src="{{ url('img/'. $abono->sucursal->empresa->logo) }}" alt="logo" width="70">
        <h1 class="text-center">{{ $abono->sucursal->empresa->nombre }}</h1>
        <p class="text-center">{{ $abono->sucursal->empresa->sector }}</p>
    </div>
    <div class="footer">
        <hr>
        <h4 class="text-center">
            {{ $abono->sucursal->empresa->nombre }} | {{ $abono->sucursal->empresa->correo }} | {{ $abono->sucursal->empresa->telefono }}
        </h4>
    </div>
    <div class="receipt">

        <h2 class="text-center">Comprobante de pago</h2>
        <p class="text-right">Fecha: {{ \Carbon\Carbon::parse($abono->fecha)->format('d/m/Y') }}</p>

        <div class="details">
            <p><label>Cliente:</label> {{ $abono->nombre_cliente }}</p>
            <p><label>Venta:</label> {{ $abono->venta_id }}</p>
            <p><label>Concepto:</label> {{ $abono->concepto }}</p>
            <p><label>Monto del Abono:</label> ${{ $abono->total }}</p>
            <p><label>Saldo:</label> ${{ $abono->venta->saldo }}</p>
            <p><label>Recibido por:</label> {{ $abono->nombre_usuario }}</p>
        </div>
    </div>
</body>
</html>
