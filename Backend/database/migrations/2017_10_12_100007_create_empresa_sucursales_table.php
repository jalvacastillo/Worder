    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaSucursalesTable extends Migration {

    public function up()
    {
        Schema::create('empresa_sucursales', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->string('telefono')->nullable();
            $table->string('correo')->nullable();
            $table->string('municipio')->nullable();
            $table->string('departamento')->nullable();
            $table->string('direccion')->nullable();
            $table->string('tipo_establecimiento')->nullable();
            $table->string('cod_estable_mh')->nullable();
            $table->integer('empresa_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa_sucursales');
    }

}
