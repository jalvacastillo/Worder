<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadoComisionesTable extends Migration {

    public function up()
    {
        Schema::create('empleado_comisiones', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->string('concepto');
            $table->string('tipo');
            $table->string('estado');
            $table->text('nota')->nullable();
            $table->decimal('total', 9,2);
            $table->integer('venta_id')->nullable();
            $table->integer('empleado_id');
            $table->integer('usuario_id');

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('empleado_comisiones');
    }

}
