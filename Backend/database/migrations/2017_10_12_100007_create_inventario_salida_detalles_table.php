<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventarioSalidaDetallesTable extends Migration {

    public function up()
    {
        Schema::create('inventario_salida_detalles', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('producto_id');
            $table->integer('cantidad');
            $table->decimal('costo', 9,2);
            $table->decimal('total', 9,2);
            $table->integer('salida_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('inventario_salida_detalles');
    }

}
