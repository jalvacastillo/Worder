<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuscripcionesTable extends Migration {

    public function up()
    {
        Schema::create('suscripciones', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('estado');
            $table->date('inicio');
            $table->date('fin');
            $table->integer('plan_id');
            $table->integer('usuario_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('suscripciones');
    }

}
