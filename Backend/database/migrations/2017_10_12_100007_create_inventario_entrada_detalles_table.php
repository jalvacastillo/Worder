<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventarioEntradaDetallesTable extends Migration {

    public function up()
    {
        Schema::create('inventario_entrada_detalles', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('producto_id');
            $table->integer('cantidad');
            $table->decimal('costo', 9,2);
            $table->decimal('total', 9,2);
            $table->integer('entrada_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('inventario_entrada_detalles');
    }

}
