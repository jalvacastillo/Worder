<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaActivosCategoriasTable extends Migration
{

    public function up()
    {
        Schema::create('empresa_activos_categorias', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('nombre');
            $table->integer('empresa_id');

            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('empresa_activos_categorias');
    }
}
