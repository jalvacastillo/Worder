<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajasTable extends Migration {

	public function up()
	{
		Schema::create('cajas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nombre');
			$table->string('tipo');
			$table->string('codigo')->nullable();
			$table->text('descripcion')->nullable();
			$table->integer('sucursal_id');

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cajas');
	}

}
