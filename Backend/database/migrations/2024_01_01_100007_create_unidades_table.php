<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesTable extends Migration {

    public function up()
    {
        Schema::create('unidades', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('cod');
            $table->string('nombre');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('unidades');
    }

}
