<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadesEconomicasTable extends Migration {

    public function up()
    {
        Schema::create('actividades_economicas', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('cod');
            $table->string('nombre');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('actividades_economicas');
    }

}
