<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{

    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('img')->default('usuarios/default.jpg');
            $table->string('nombre');
            $table->date('fecha_nacimiento')->nullable();
            $table->string('genero')->nullable();
            $table->string('dui')->nullable();
            $table->string('telefono')->nullable();
            $table->string('correo')->unique()->nullable();;
            $table->string('municipio')->nullable();
            $table->string('departamento')->nullable();
            $table->string('direccion')->nullable();
            $table->string('pais')->nullable();
            $table->string('nacionalidad')->nullable();
            $table->string('num_licencia')->nullable();
            $table->date('fecha_vencimiento')->nullable();
            $table->string('tipo_licencia')->nullable();
            $table->integer('cargo_id')->nullable();
            $table->date('fecha_inicio')->nullable();
            $table->string('estado')->nullable();
            $table->decimal('sueldo', 9,2)->nullable();
            $table->string('tipo_salario')->nullable();
            $table->boolean('renta')->default(0);
            $table->boolean('isss')->default(0);
            $table->boolean('afp')->default(0);
            
            $table->string('tipo_comision')->default('Ninguna');
            $table->decimal('comision', 6,2)->default(0);
            
            $table->string('contacto_nombre')->nullable();
            $table->string('contacto_telefono')->nullable();
            $table->text('nota')->nullable();
            $table->boolean('activo')->default(1);
            $table->integer('sucursal_id');

            $table->softDeletes();
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
