<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosMetasTable extends Migration {

    public function up()
    {
        Schema::create('empleados_metas', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('mes');
            $table->integer('ano');
            $table->decimal('meta', 9,2);
            $table->string('tipo_comision')->nullable();
            $table->decimal('comision', 9,2)->nullable();
            $table->string('nota')->nullable();
            $table->integer('empleado_id');

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('empleados_metas');
    }

}
