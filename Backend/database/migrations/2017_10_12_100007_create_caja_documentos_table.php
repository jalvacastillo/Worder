<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajaDocumentosTable extends Migration {

    public function up()
    {
        Schema::create('caja_documentos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->string('prefijo')->nullable();
            $table->integer('actual');
            $table->integer('inicial')->nullable();
            $table->integer('final')->nullable();
            $table->string('rangos')->nullable();
            $table->string('numero_autorizacion')->nullable();
            $table->string('resolucion')->nullable();
            $table->date('fecha')->nullable();
            $table->string('nota')->nullable();
            $table->integer('caja_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('caja_documentos');
    }

}
