<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosAsistenciasTable extends Migration
{

    public function up()
    {
        Schema::create('empleados_asistencias', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('empleado_id');
            $table->date('fecha');
            $table->time('entrada')->nullable();
            $table->time('salida')->nullable();
            $table->string('estado')->default('Asistencia');
            $table->string('nota')->nullable();

            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('empleados_asistencias');
    }
}
