<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventarioAjustesTable extends Migration {

	public function up()
	{
		Schema::create('inventario_ajustes', function(Blueprint $table)
		{
			$table->increments('id');

			$table->text('nota')->nullable();
			$table->integer('producto_id');
			$table->integer('bodega_id');
			$table->decimal('stock_inicial', 9,2);
			$table->decimal('stock_final', 9,2);
			$table->integer('usuario_id');

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('inventario_ajustes');
	}

}
