<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosDocumentosTable extends Migration {

    public function up()
    {
        Schema::create('empleado_documentos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre')->nullable();
            $table->string('url');
            $table->string('tamano')->nullable();
            $table->string('tipo')->nullable();
            $table->integer('empleado_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empleado_documentos');
    }

}
