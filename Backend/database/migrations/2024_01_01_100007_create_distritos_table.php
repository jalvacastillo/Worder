<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistritosTable extends Migration {

    public function up()
    {
        Schema::create('distritos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('cod');
            $table->string('nombre');
            $table->string('cod_municipio');
            $table->string('cod_departamento');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('distritos');
    }

}
