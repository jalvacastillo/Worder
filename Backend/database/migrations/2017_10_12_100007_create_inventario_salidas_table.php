<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventarioSalidasTable extends Migration {

    public function up()
    {
        Schema::create('inventario_salidas', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->integer('bodega_id');
            $table->string('concepto');
            $table->string('tipo')->nullable();
            $table->integer('usuario_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('inventario_salidas');
    }

}
