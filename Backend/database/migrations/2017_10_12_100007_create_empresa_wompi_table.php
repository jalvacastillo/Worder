    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaWompiTable extends Migration {

    public function up()
    {
        Schema::create('empresa_wompi', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('identificador');
            $table->string('aplicativo')->nullable();
            $table->string('secret')->nullable();
            $table->integer('empresa_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa_wompi');
    }

}
