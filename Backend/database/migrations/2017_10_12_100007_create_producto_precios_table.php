<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoPreciosTable extends Migration {

    public function up()
    {
        Schema::create('producto_precios', function(Blueprint $table)
        {
            $table->increments('id');

            $table->decimal('precio', 9,2);
            $table->integer('producto_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('producto_precios');
    }

}
