    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaPresupuestosTable extends Migration {

    public function up()
    {
        Schema::create('empresa_presupuestos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->date('inicio');
            $table->date('fin');
            $table->decimal('ingresos', 10,2);
            $table->decimal('egresos', 10,2);
            $table->decimal('alquiler')->default(0);
            $table->decimal('varios')->default(0);
            $table->decimal('mantenimiento')->default(0);
            $table->decimal('marketing')->default(0);
            $table->decimal('materia_prima')->default(0);
            $table->decimal('comisiones')->default(0);
            $table->decimal('combustible')->default(0);
            $table->decimal('planilla')->default(0);
            $table->decimal('servicios')->default(0);
            $table->decimal('prestamos')->default(0);
            $table->decimal('publicidad')->default(0);
            $table->integer('empresa_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa_presupuestos');
    }

}
