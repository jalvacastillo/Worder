    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration {

    public function up()
    {
        Schema::create('empresas', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->string('nombre_comercial');
            $table->string('nombre_propietario')->nullable();
            $table->string('sector')->nullable();
            $table->string('actividad_economica')->nullable();
            $table->string('tipo_establecimiento')->nullable();
            $table->string('nit')->nullable();
            $table->string('nrc')->nullable();
            $table->string('tipo_contribuyente')->nullable();
            $table->string('telefono')->nullable();
            $table->string('correo')->nullable();
            $table->string('logo')->default('empresas/default.jpg');
            
            $table->string('departamento')->nullable();
            $table->string('municipio')->nullable();
            $table->string('distrito')->nullable();
            $table->string('direccion')->nullable();
            $table->string('pais')->nullable();
            $table->boolean('facturacion_electronica')->default(0);
            $table->boolean('enviar_dte')->default(0);
            $table->boolean('imprimir_dte')->default(0);
            $table->string('fe_ambiente')->nullable();
            $table->string('cod_municipio')->nullable();
            $table->string('cod_distrito')->nullable();
            $table->string('cod_departamento')->nullable();
            $table->string('cod_actividad_economica')->nullable();
            $table->string('mh_pwd_certificado')->nullable();
            $table->string('mh_usuario')->nullable();
            $table->string('mh_contrasena')->nullable();
            $table->string('cod_estable_mh')->nullable();
            $table->string('cod_estable')->nullable();
            $table->string('moneda')->default('USD');
            $table->boolean('impuesto')->default(0);
            $table->boolean('editar_precio_venta')->default(0);
            $table->boolean('vender_sin_stock')->default(0);
            $table->string('valor_inventario')->default('Promedio');
            $table->string('ips', 255)->nullable();
            $table->integer('bodega_compra_id')->default(1);
            $table->integer('bodega_venta_id')->default(2);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresas');
    }

}
