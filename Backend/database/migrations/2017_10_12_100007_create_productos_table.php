<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration {

	public function up()
	{
		Schema::create('productos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nombre');
			$table->text('descripcion')->nullable();
			$table->string('codigo')->nullable();
			$table->string('sku')->nullable();
			$table->string('medida')->default('Unidad');
			$table->decimal('precio', 9,2)->default(0);

			$table->decimal('costo', 9,2)->default(0);
			$table->decimal('costo_anterior', 9,2)->default(0);
			
			$table->integer('categoria_id')->nullable();
			$table->integer('subcategoria_id')->nullable();
			$table->string('marca')->nullable();
			$table->string('etiquetas')->nullable();

			$table->string('tipo')->nullable();
			$table->decimal('impuesto', 9,2)->default('0.13');
            $table->string('tipo_impuesto')->default('Gravada');

            $table->string('tipo_comision')->default('Ninguna');
            $table->decimal('comision', 6,2)->default(0);

			$table->boolean('activo')->default(1);
			$table->text('nota')->nullable();
			$table->integer('empresa_id');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('productos');
	}

}
