    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosPlanillasTable extends Migration {

    public function up()
    {
        Schema::create('empleados_planillas', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->string('tipo');
            $table->string('estado');
            $table->decimal('sueldo_base',9,2)->default(0);
            $table->decimal('horas_extras',9,2)->default(0);
            $table->decimal('comisiones',9,2)->default(0);
            $table->decimal('otros_ingresos',9,2)->default(0);
            $table->decimal('bonificaciones',9,2)->default(0);
            $table->decimal('vacaciones',9,2)->default(0);
            $table->decimal('indemnizacion',9,2)->default(0);
            $table->decimal('aguinaldo',9,2)->default(0);
            $table->decimal('total_bruto',9,2)->default(0);
            $table->decimal('isss_patronal',9,2)->default(0);
            $table->decimal('afp_patronal',9,2)->default(0);
            $table->decimal('isss',9,2)->default(0);
            $table->decimal('afp',9,2)->default(0);
            $table->decimal('renta',9,2)->default(0);
            $table->decimal('anticipos',9,2)->default(0);
            $table->decimal('prestamos',9,2)->default(0);
            $table->decimal('institucion_financiera',9,2)->default(0);
            $table->decimal('otros_descuentos',9,2)->default(0);
            $table->decimal('total_descuentos',9,2)->default(0);
            $table->decimal('total',9,2)->default(0);
            $table->text('nota')->nullable();
            $table->integer('usuario_id');
            $table->integer('empresa_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empleados_planillas');
    }

}
