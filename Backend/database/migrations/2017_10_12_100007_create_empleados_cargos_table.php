<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosCargosTable extends Migration
{

    public function up()
    {
        Schema::create('empleados_cargos', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('nombre');
            $table->integer('empresa_id');

            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('empleados_cargos');
    }
}
