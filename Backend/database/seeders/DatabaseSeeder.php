<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            EmpresaTableSeeder::class,
            UsersTableSeeder::class,
            MHTableSeeder::class,
            CategoriasTableSeeder::class,
            ClientesTableSeeder::class,
            ProveedoresTableSeeder::class,
            ProductosTableSeeder::class,
            EmpleadosTableSeeder::class,
            // FlotasTableSeeder::class,
            // FletesTableSeeder::class,
            // MantenimientosTableSeeder::class,

            // OrdenesTableSeeder::class,
            VentasTableSeeder::class,
            // ComprasTableSeeder::class,
            // GastosTableSeeder::class,

        ]);
    }
}
        
