import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';
import { MHService } from '@services/MH.service';

declare let $:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

    public user: any = {};
    public loading = false;
    public saludo:string = '';
    public anio:any = '';
    public showpassword:boolean = false;

    constructor( private apiService: ApiService, private mhService: MHService,
                private router: Router, private alertService: AlertService) { }

    ngOnInit() {
        this.saludo = this.apiService.saludar();
        this.anio = new Date().getFullYear();
        this.apiService.logout();
    }

    submit() {
        this.loading = true;
        this.user.username = this.user.username.toLowerCase();
        // this.user.password = this.user.password.toLowerCase();

        this.apiService.login(this.user)
        .subscribe(
            data => {

                this.user = this.apiService.auth_user();

                if(this.user.empresa.fe_ambiente == '01'){
                    localStorage.setItem('worder_mh_url_base', 'https://api.dtes.mh.gob.sv');
                }else{
                    localStorage.setItem('worder_mh_url_base', 'https://apitest.dtes.mh.gob.sv');
                }

                if(this.user.empresa.mh_usuario && this.user.empresa.mh_contrasena){
                    this.mhService.login();
                }

                setTimeout(()=>{
                    this.apiService.loadData();
                },2000);

                this.router.navigate(['/']);
                this.loading = false;
            },
            error => {
                $('.container').addClass("animated shake");
                this.alertService.error(error);
                this.loading = false;
            });
    }

    public mostrarPassword(){
        this.showpassword = !this.showpassword;
        console.log(this.showpassword);
    }  

}
