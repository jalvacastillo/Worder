import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'app-cliente-informacion',
  templateUrl: './cliente-informacion.component.html'
})
export class ClienteInformacionComponent implements OnInit {

    public cliente: any = {};
    public loading = false;
    public saving = false;
    public filtro:any = {};
    public departamentos:any = [];
    public municipios:any = [];
    public distritos:any = [];
    public actividad_economicas:any = [];

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private route: ActivatedRoute, private router: Router
    ) { }

    ngOnInit() {
        
        const id = +this.route.snapshot.paramMap.get('id')!;
        this.filtro.estado = "";
        if(this.route.snapshot.paramMap.get('estado'))
            this.filtro.estado = this.route.snapshot.paramMap.get('estado');
            
        if(isNaN(id)){
            this.cliente = {};
            this.cliente.usuario_id = this.apiService.auth_user().id;
        }
        else{
            // Optenemos el cliente
            this.loading = true; 
            this.apiService.read('cliente/', id).subscribe(cliente => {
               this.cliente = cliente;
               this.loading = false; 
            },error => {this.alertService.error(error); this.loading = false; });
        }

        this.departamentos = JSON.parse(localStorage.getItem('departamentos')!);
        this.municipios = JSON.parse(localStorage.getItem('municipios')!);
        this.distritos = JSON.parse(localStorage.getItem('distritos')!);
        this.actividad_economicas = JSON.parse(localStorage.getItem('actividad_economicas')!);

    }

    setGiro(){
        this.cliente.giro = this.actividad_economicas.find((item:any) => item.cod == this.cliente.cod_giro).nombre;
    }

    setDistrito(){
        let distrito = this.distritos.find((item:any) => item.cod == this.cliente.cod_distrito && item.cod_departamento == this.cliente.cod_departamento);
        
        if(distrito){
            this.cliente.cod_municipio = distrito.cod_municipio;
            this.setMunicipio();
            this.cliente.distrito = distrito.nombre; 
            this.cliente.cod_distrito = distrito.cod;
        }
    }

    setMunicipio(){
        let municipio = this.municipios.find((item:any) => item.cod == this.cliente.cod_municipio && item.cod_departamento == this.cliente.cod_departamento);
        if(municipio){
            this.cliente.municipio = municipio.nombre; 
            this.cliente.cod_municipio = municipio.cod;

            this.cliente.distrito = ''; 
            this.cliente.cod_distrito = '';
        }
    }

    setDepartamento(){
        let departamento = this.departamentos.find((item:any) => item.cod == this.cliente.cod_departamento);
        if(departamento){
            this.cliente.departamento = departamento.nombre; 
            this.cliente.cod_departamento = departamento.cod;

        }
        this.cliente.municipio = ''; 
        this.cliente.cod_municipio = '';
        this.cliente.distrito = ''; 
        this.cliente.cod_distrito = '';
    }

    public onSubmit() {
        this.saving = true;
        // Guardamos al cliente
        this.apiService.store('cliente', this.cliente).subscribe(cliente => {
            this.cliente = cliente;
            this.saving = false;
            this.alertService.success('Cliente guardado');
            this.router.navigate(['/cliente/'+ cliente.id]);
        },error => {
            this.alertService.error(error);
            this.saving = false;
        });
    }

}
