import { Component, OnInit, TemplateRef } from '@angular/core';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ApiService } from '../../../services/api.service';
import { AlertService } from '../../../services/alert.service';

@Component({
  selector: 'app-abonos',
  templateUrl: './abonos.component.html',
})
export class AbonosComponent implements OnInit {

    public abonos:any;
    public loading:boolean = false;

    public filtro:any = {};
    public filtrado:boolean = false;
    public usuarios:any = [];
    public sucursales:any = [];
    
    modalRef?: BsModalRef;

    constructor( public apiService:ApiService, private alertService:AlertService, private modalService: BsModalService ){}

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('abonos').subscribe(abonos => { 
            this.abonos = abonos;
            this.loading = false; this.filtrado = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    public search(buscador:any){
        if(buscador && buscador.length > 2) {
            this.loading = true;
            this.apiService.read('abonos/buscar/', buscador).subscribe(abonos => { 
                this.abonos = abonos;
                this.loading = false;
            }, error => {this.alertService.error(error); this.loading = false;});
        }else{
            this.loadAll();
        }
    }

    public delete(abono:any){
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('abono/', abono.id) .subscribe(data => {
                for (let i = 0; i < this.abonos.data.length; i++) { 
                    if (this.abonos.data[i].id == data.id )
                        this.abonos.data.splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }
    }

    public setEstado(abono:any, estado:any):void{
        this.loading = true;
        abono.estado = estado;
        this.apiService.store('abono', abono).subscribe(data => {
            this.loading = false;
            abono = data;
            this.alertService.success('Guardado');
        },error => {this.alertService.error(error); this.loading = false; });
    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.abonos.path + '?page='+ event.page).subscribe(abonos => { 
            this.abonos = abonos;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }


    // Filtros
    openFilter(template: TemplateRef<any>) {     

        if(!this.filtrado) {
            this.filtro.inicio = this.apiService.date();
            this.filtro.fin = this.apiService.date();
            this.filtro.sucursal_id = '';
            this.filtro.usuario_id = '';
            this.filtro.estado = '';
            this.filtro.metodo_pago = '';
            this.filtro.tipo_documento = '';
        }
        if(!this.sucursales.data){
            this.apiService.getAll('sucursales').subscribe(sucursales => { 
                this.sucursales = sucursales;
            }, error => {this.alertService.error(error); });
        }
        this.modalRef = this.modalService.show(template);
    }

    onFiltrar(){
        this.loading = true;
        this.apiService.store('abonos/filtrar', this.filtro).subscribe(abonos => { 
            this.abonos = abonos;
            this.loading = false; this.filtrado = true;
            this.modalRef!.hide();
        }, error => {this.alertService.error(error); this.loading = false;});

    }

    public imprimirDoc(abono:any){
        setTimeout(()=>{
            window.open(this.apiService.baseUrl + '/api/reporte/abono/' + abono.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
        }, 1000);
    }


}
