import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SumPipe }     from '@pipes/sum.pipe';
import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'app-abono',
  templateUrl: './abono.component.html',
  providers: [ SumPipe ]
})

export class AbonoComponent implements OnInit {

    public abono: any= {};
    public venta: any= {};
    public clientes:any = [];
    public ventas:any = [];
    public loading = false;
    public saving = false;
    modalRef!: BsModalRef;
    
	constructor( 
	    public apiService: ApiService, private alertService: AlertService,
	    private modalService: BsModalService, private sumPipe:SumPipe,
        private route: ActivatedRoute, private router: Router,
	) {
        this.router.routeReuseStrategy.shouldReuseRoute = function() {return false; };
    }

	ngOnInit() {

        const id = +this.route.snapshot.queryParamMap.get('id')!;

        if(id == 0){
            this.cargarDatosIniciales();
        }
        else{
            this.loading = true;
            this.apiService.read('abono/', id).subscribe(abono => {
                this.abono = abono;
                this.loading = false;
            }, error => {this.alertService.error(error);this.loading = false;});
        }

        this.apiService.getAll('clientes/list').subscribe(clientes => {
            this.clientes = clientes;
        }, error => {this.alertService.error(error);});

    }

    cargarDatosIniciales(){
        this.abono = {};
        this.abono.fecha = this.apiService.date();
        this.abono.estado = 'Pagado';
        this.abono.metodo_pago = 'Efectivo';
        this.abono.usuario_id = this.apiService.auth_user().id;
        this.abono.sucursal_id = this.apiService.auth_user().sucursal_id;

        let corte = JSON.parse(sessionStorage.getItem('worder_corte')!);
        if (corte) {
            this.abono.fecha = JSON.parse(sessionStorage.getItem('worder_corte')!).fecha;
            this.abono.caja_id = JSON.parse(sessionStorage.getItem('worder_corte')!).caja_id;
            this.abono.corte_id = JSON.parse(sessionStorage.getItem('worder_corte')!).id;
        }
    }

    public selectCliente(){
        this.loading = true;
        this.apiService.getAll('cuentas-cobrar/' + this.abono.cliente_id).subscribe(ventas => {
            this.ventas = ventas;
            if (this.ventas.length > 0) {
                this.selectVenta(this.ventas[0]);
            }
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    public selectVenta(venta:any){
        this.venta = venta;
        this.abono.venta_id = venta.id;
        console.log(this.venta);
    }

    public onSubmit() {
        this.saving = true;
        if (!this.abono.concepto) {
            this.abono.concepto = 'Abono a venta #' + this.abono.venta_id;
        }
        this.apiService.store('abono', this.abono).subscribe(abono => {
            this.saving = false;
            this.imprimirDoc(abono);
            this.router.navigate(['/abonos']);
            this.alertService.success("Guardado");
        },error => {this.alertService.error(error); this.saving = false; });
    }


    public imprimirDoc(abono:any){
        setTimeout(()=>{
            window.open(this.apiService.baseUrl + '/api/reporte/abono/' + abono.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
        }, 1000);
    }


}
