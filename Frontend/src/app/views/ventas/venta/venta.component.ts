import { Component, OnInit,TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SumPipe }     from '../../../pipes/sum.pipe';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html'
})
export class VentaComponent implements OnInit {

    public venta:any = {};

    public loading = false;

    constructor( public apiService:ApiService, private alertService:AlertService, private sumPipe:SumPipe,
        private route: ActivatedRoute, private router: Router, private modalService: BsModalService,
    ) {
        // this.router.routeReuseStrategy.shouldReuseRoute = function() {return false; };
    }

    ngOnInit() {

        this.loadAll();

    }

    public loadAll(){
        this.venta.id = +this.route.snapshot.paramMap.get('id')!;
        this.loading = true;
        this.apiService.read('venta/', this.venta.id).subscribe(venta => {
        this.venta = venta;
        this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});

    }

    public delete(abono:any){
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('abono/', abono.id) .subscribe(data => {
                for (let i = 0; i < this.venta.abonos.length; i++) { 
                    if (this.venta.abonos[i].id == data.id )
                        this.venta.abonos.splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }
    }

    public planDePagos(){
        window.open(this.apiService.baseUrl + '/api/reporte/credito/pagos/' + this.venta.credito.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
    }

    public planDeAmortizacion(){
        window.open(this.apiService.baseUrl + '/api/reporte/credito/plan-de-pagos/' + this.venta.credito.total + '/'  + this.venta.credito.numero_de_cuotas + '/'  + this.venta.credito.forma_de_pago + '/'  + this.venta.credito.interes_anual + '/'  + this.venta.credito.prima + '/'  + this.venta.credito.periodo_de_gracia + '/'  + this.venta.credito.id + '/' +  '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
    }

    public imprimirRecibo(abono:any){
        setTimeout(()=>{
            window.open(this.apiService.baseUrl + '/api/reporte/abono/' + abono.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
        }, 1000);
    }


}
