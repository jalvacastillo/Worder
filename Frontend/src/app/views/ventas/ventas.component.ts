import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';
import { MHService } from '@services/MH.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html'
})

export class VentasComponent implements OnInit {

    public ventas:any = [];
    public venta:any = {};
    public buscador:any = '';
    public loading:boolean = false;
    public saving:boolean = false;
    public sending:boolean = false;
    public consulting:boolean = false;

    public clientes:any = [];
    public usuarios:any = [];
    public sucursales:any = [];
    public documentos:any = [];
    public filtro:any = {};
    public filtrado:boolean = false;
    public dte:any = {};

    modalRef!: BsModalRef;

    constructor(
        public mhService: MHService, public apiService: ApiService, private alertService: AlertService, 
        private modalService: BsModalService
    ){}

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('ventas').subscribe(ventas => { 
            this.ventas = ventas;
            this.loading = false;this.filtrado = false;
        }, error => {this.alertService.error(error); });
    }

    public search(){
        if(this.buscador && this.buscador.length > 1) {
            this.loading = true;
            this.apiService.read('ventas/buscar/', this.buscador).subscribe(ventas => { 
                this.ventas = ventas;
                this.loading = false;this.filtrado = true;
            }, error => {this.alertService.error(error); this.loading = false;this.filtrado = false; });
        }
    }

    public setEstado(venta:any, estado:string){
        venta.estado = estado;
        if (estado == 'Pagada') {
            venta.fecha_pago = this.apiService.date();
        }
        this.apiService.store('venta', venta).subscribe(venta => { 
            this.alertService.success('Venta ' + estado);
        }, error => {this.alertService.error(error); });
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('venta/', id) .subscribe(data => {
                for (let i = 0; i < this.ventas['data'].length; i++) { 
                    if (this.ventas['data'][i].id == data.id )
                        this.ventas['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public filtrar(filtro:any, txt:any){
        this.loading = true;
        this.apiService.read('ventas/filtrar/' + filtro + '/', txt).subscribe(ventas => { 
            this.ventas = ventas;
            this.loading = false;
        }, error => {this.alertService.error(error); });

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.ventas.path + '?page='+ event.page).subscribe(ventas => { 
            this.ventas = ventas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    public reemprimir(venta:any){
        window.open(this.apiService.baseUrl + '/api/reporte/facturacion/' + venta.id + '?token=' + this.apiService.auth_token(), 'Impresión', 'width=400');
    }

    // Editar

    openModalEdit(template: TemplateRef<any>, venta:any) {
        this.venta = venta;
        
        this.apiService.getAll('documentos').subscribe(documentos => {
            this.documentos = documentos;
        }, error => {this.alertService.error(error);});

        this.modalRef = this.modalService.show(template);
    }

    public onSubmit() {
        this.loading = true;            
        this.apiService.store('venta', this.venta).subscribe(venta => {
            this.venta = {};
            this.modalRef.hide();
            this.loading = false;
            this.alertService.success("Guardado");
        },error => {this.alertService.error(error); this.loading = false; });

    }

    // Filtros

    openFilter(template: TemplateRef<any>) {     

        if(!this.filtrado) {
            this.filtro.inicio = this.apiService.date();
            this.filtro.fin = this.apiService.date();
            this.filtro.sucursal_id = '';
            this.filtro.usuario_id = '';
            this.filtro.estado = '';
            this.filtro.metodo_pago = '';
            this.filtro.tipo_documento = '';
        }
        if(!this.usuarios.data){
            this.apiService.store('usuarios/filtrar', {tipo: 'Vendedor'}).subscribe(usuarios => { 
                this.usuarios = usuarios.data;
            }, error => {this.alertService.error(error); });
        }
        if(!this.sucursales.data){
            this.apiService.getAll('sucursales').subscribe(sucursales => { 
                this.sucursales = sucursales;
            }, error => {this.alertService.error(error); });
        }
        this.modalRef = this.modalService.show(template);
    }

    onFiltrar(){
        this.loading = true;
        this.apiService.store('ventas/filtrar', this.filtro).subscribe(ventas => { 
            this.ventas = ventas;
            this.loading = false; this.filtrado = true;
            this.modalRef.hide();
        }, error => {this.alertService.error(error); this.loading = false;});

    }

    openDTE(template: TemplateRef<any>, venta:any){
        this.venta = venta;
        this.modalRef = this.modalService.show(template);
        if(!this.venta.sello_mh){
            this.emitirDTE();
        }
    }

    imprimirDTEPDF(venta:any){
        window.open(this.apiService.baseUrl + '/api/reporte/dte/' + venta.id + '/' + venta.tipo_dte +'?token=' + this.apiService.auth_token(), 'hola', 'width=400');
    }

    imprimirDTEJSON(venta:any){
        window.open(this.apiService.baseUrl + '/api/reporte/dte-json/' + venta.id + '/' + venta.tipo_dte +'?token=' + this.apiService.auth_token(), 'hola', 'width=400');
    }

    emitirDTE(){
        this.saving = true;
        this.mhService.emitirDTE(this.venta).then((venta) => {
            this.venta = venta;
            this.alertService.success('DTE emitido. El documento ha sido emitido.');
            if(this.venta.cliente_id){
                this.enviarDTE();
            }
            this.saving = false;
        }).catch((error) => {
            this.saving = false;
            if(error == '[identificacion.codigoGeneracion] YA EXISTE UN REGISTRO CON ESE VALOR'){
                this.consultarDTE();
            }else{
                this.alertService.warning(error);
            }
        });
    }

    consultarDTE(){
        this.consulting = true;
        let data = {
            codigoGeneracion: this.venta.dte.identificacion.codigoGeneracion,
            fechaEmi: this.venta.dte.identificacion.fecEmi,
            ambiente: this.venta.dte.identificacion.ambiente
        };

        setTimeout(()=>{

            this.apiService.store('consultarDTE', data).subscribe(dte => {
                if (dte && dte.selloVal) {
                    this.venta.dte.sello = dte.selloVal;
                    this.venta.sello_mh = dte.selloVal;
                    this.apiService.store('venta', this.venta).subscribe(data => {
                        this.alertService.success('Sello recibido. El DTE ha sido sellado.');
                        if(this.venta.cliente_id){
                            this.enviarDTE();
                        }
                        setTimeout(()=>{
                            this.modalRef?.hide();
                        },500);
                        this.consulting = false;
                    },error => {this.alertService.error(error);});
                }
                else if (dte){
                    this.consulting = false;
                    this.alertService.info('El DTE no ha sido emitido.');
                }else {
                    this.consulting = false;
                    this.alertService.info('Hacienda no devolvió el sello.');
                }
            }, error => {
                this.consulting = false;
                this.alertService.warning(error);
            });
        },1000);
    }

    enviarDTE(){
        this.sending = true;
        this.apiService.store('enviarDTE', this.venta).subscribe(dte => {
            this.alertService.success('DTE enviado. El DTE fue enviado por correo al cliente.');
            this.sending = false;
            setTimeout(()=>{
                this.modalRef?.hide();
            },1000);
        },error => {this.alertService.error(error); this.sending = false; });
    }

    emitirEnContingencia(venta:any){
        this.venta = venta;
        this.saving = true;
        this.mhService.emitirDTEContingencia(this.venta).then((venta) => {
            this.venta = venta;
            this.alertService.success('DTE emitido. El documento ha sido emitido.');
            this.saving = false;
        }).catch((error) => {
            this.saving = false;
            this.alertService.warning(error);
        });
    }

    anularDTE(venta:any){
        if (confirm('¿Confirma anular la venta y el DTE?')) {
            this.venta = venta;
            this.saving = true;
            this.apiService.store('generarDTEAnulado', this.venta).subscribe(dte => {
                // this.alertService.success('DTE generado.');
                this.venta.dte_invalidacion = dte;
                this.mhService.firmarDTE(dte).subscribe(dteFirmado => {
                    this.venta.dte_invalidacion.firmaElectronica = dteFirmado.body;
                    // this.alertService.success('DTE firmado.');
                    
                    this.mhService.anularDTE(this.venta, dteFirmado.body).subscribe(dte => {
                        if ((dte.estado == 'PROCESADO') && dte.selloRecibido) {
                            this.venta.dte_invalidacion.sello = dte.selloRecibido;
                            this.venta.estado = 'Anulada';
                            this.apiService.store('venta', this.venta).subscribe(data => {
                                this.enviarDTE();
                            },error => {this.alertService.error(error); this.saving = false; });
                        }

                        this.alertService.success('DTE anulado.');
                    },error => {
                        if(error.error.descripcionMsg){
                            this.alertService.warning(error.error.descripcionMsg);
                        }
                        if(error.error.observaciones.length > 0){
                            this.alertService.warning(error.error.observaciones);
                        }
                        this.saving = false;
                    });

                },error => {this.alertService.error(error);this.saving = false; });

            },error => {this.alertService.error(error);this.saving = false; });
        }
    }

    async emitirDTEs() {
        for (const item of this.ventas.data) {
            if(!item.sello_mh){
                this.venta = item; // Asignas la venta actual
                this.emitirDTE(); // Llamas a emitirDTE
                
                // Determinas el retraso según la condición
                const delayTime = this.venta.cliente_id ? 10000 : 6000;
                await this.delay(delayTime);
            }
        }
    }

    delay(ms: number): Promise<void> {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

}
