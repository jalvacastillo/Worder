import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';
import { MHService } from '@services/MH.service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html'
})
export class EmpresaComponent implements OnInit {

    public empresa: any = {};
    public municipios: any = {};
    public departamentos: any = {};
    public distritos: any = {};
    public actividad_economicas: any = {};
    public loading = false;
    public saving = false;
    public cheking = false;

    constructor( 
        public apiService: ApiService, public mhService: MHService, private alertService: AlertService,
        private route: ActivatedRoute, private router: Router
    ) { }

    ngOnInit() {

        this.departamentos = JSON.parse(localStorage.getItem('departamentos')!);
        this.municipios = JSON.parse(localStorage.getItem('municipios')!);
        this.distritos = JSON.parse(localStorage.getItem('distritos')!);
        this.actividad_economicas = JSON.parse(localStorage.getItem('actividad_economicas')!);

        this.loading = true;
        this.apiService.read('empresa/', 1).subscribe(empresa => {
            this.empresa = empresa;
            this.loading = false;
        },error => {this.alertService.error(error); this.loading = false; });

    }

    setGiro(){
        this.empresa.actividad_economica = this.actividad_economicas.find((item:any) => item.cod == this.empresa.cod_actividad_economica).nombre;
    }

    setDistrito(){
        let distrito = this.distritos.find((item:any) => item.cod == this.empresa.cod_distrito && item.cod_departamento == this.empresa.cod_departamento);
        
        if(distrito){
            this.empresa.cod_municipio = distrito.cod_municipio;
            this.setMunicipio();
            this.empresa.distrito = distrito.nombre; 
            this.empresa.cod_distrito = distrito.cod;
        }
    }

    setMunicipio(){
        let municipio = this.municipios.find((item:any) => item.cod == this.empresa.cod_municipio && item.cod_departamento == this.empresa.cod_departamento);
        if(municipio){
            this.empresa.municipio = municipio.nombre; 
            this.empresa.cod_municipio = municipio.cod;

            this.empresa.distrito = ''; 
            this.empresa.cod_distrito = '';
        }
    }

    setDepartamento(){
        let departamento = this.departamentos.find((item:any) => item.cod == this.empresa.cod_departamento);
        if(departamento){
            this.empresa.departamento = departamento.nombre; 
            this.empresa.cod_departamento = departamento.cod;

        }
        this.empresa.municipio = ''; 
        this.empresa.cod_municipio = '';
        this.empresa.distrito = ''; 
        this.empresa.cod_distrito = '';
    }

    public onSubmit(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.saving = true;
            this.apiService.store('empresa', this.empresa).subscribe(empresa => {
                this.empresa = empresa;

                let user:any = {}; 
                user = JSON.parse(localStorage.getItem('worder_auth_user')!);
                user.empresa = empresa;
                localStorage.setItem('worder_auth_user', JSON.stringify(user));

                this.alertService.success("Datos guardados");
                this.saving = false;
                resolve(null);
            },error => {this.alertService.error(error); this.saving = false; resolve(null);});
            
        });
    }

    public onCheckMH():void {
        this.cheking = true;
        
        this.onSubmit().then(() => {
            this.mhService.auth().subscribe(response => {
                this.cheking = false;

                if(response.status == 'ERROR'){
                    this.alertService.info(response.body.descripcionMsg);
                }else{
                    this.alertService.success('Conección exitosa: El proceso se realizo correctamente.');
                }
            },error => {this.alertService.error(error); this.cheking = false; });
        });

    }

    public onCheckFE() {
        this.cheking = true;
        
        this.mhService.verificarFirmador().subscribe(response => {
            this.cheking = false;
            console.log(response.status)
            if (response.status === 200) {
              this.alertService.success('La solicitud fue exitosa.');
            } else {
              this.alertService.warning('No se pudo conectar al firmador');
            };
        },error => {this.alertService.error(error); this.cheking = false; });

    }


}
