import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html'
})
export class ProveedorComponent implements OnInit {

    public proveedor: any = {};
    public loading = false;
    public saving = false;
    public filtro:any = {};
    public departamentos:any = [];
    public municipios:any = [];
    public distritos:any = [];
    public actividad_economicas:any = [];

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private route: ActivatedRoute, private router: Router
    ) { }

    ngOnInit() {
        
        this.loadAll();

        this.departamentos = JSON.parse(localStorage.getItem('departamentos')!);
        this.municipios = JSON.parse(localStorage.getItem('municipios')!);
        this.distritos = JSON.parse(localStorage.getItem('distritos')!);
        this.actividad_economicas = JSON.parse(localStorage.getItem('actividad_economicas')!);

    }

    setGiro(){
        this.proveedor.giro = this.actividad_economicas.find((item:any) => item.cod == this.proveedor.cod_giro).nombre;
    }

    setDistrito(){
        let distrito = this.distritos.find((item:any) => item.cod == this.proveedor.cod_distrito && item.cod_departamento == this.proveedor.cod_departamento);
        
        if(distrito){
            this.proveedor.cod_municipio = distrito.cod_municipio;
            this.setMunicipio();
            this.proveedor.distrito = distrito.nombre; 
            this.proveedor.cod_distrito = distrito.cod;
        }
    }

    setMunicipio(){
        let municipio = this.municipios.find((item:any) => item.cod == this.proveedor.cod_municipio && item.cod_departamento == this.proveedor.cod_departamento);
        if(municipio){
            this.proveedor.municipio = municipio.nombre; 
            this.proveedor.cod_municipio = municipio.cod;

            this.proveedor.distrito = ''; 
            this.proveedor.cod_distrito = '';
        }
    }

    setDepartamento(){
        let departamento = this.departamentos.find((item:any) => item.cod == this.proveedor.cod_departamento);
        if(departamento){
            this.proveedor.departamento = departamento.nombre; 
            this.proveedor.cod_departamento = departamento.cod;

        }
        this.proveedor.municipio = ''; 
        this.proveedor.cod_municipio = '';
        this.proveedor.distrito = ''; 
        this.proveedor.cod_distrito = '';
    }

    public loadAll(){
        const id = +this.route.snapshot.paramMap.get('id')!;
        this.filtro.estado = "";
        if(this.route.snapshot.paramMap.get('estado'))
            this.filtro.estado = this.route.snapshot.paramMap.get('estado');
            
        if(isNaN(id)){
            this.proveedor = {};
        }
        else{
            this.loading = true;
            this.apiService.read('proveedor/', id).subscribe(proveedor => {
                this.proveedor = proveedor;
                this.loading = false;
            },error => {this.alertService.error(error); this.loading = false; });
        }
    }

    public onSubmit() {
        this.saving = true;
        // Guardamos al proveedor
        this.apiService.store('proveedor', this.proveedor).subscribe(proveedor => {
            this.proveedor = proveedor;
            this.saving = false;
            this.alertService.success('Proveedor guardado');
            this.router.navigate(['/proveedor/'+ proveedor.id]);
        },error => {this.alertService.error(error); this.saving = false; });
    }

}
