import { Component, OnInit,TemplateRef, ViewChild, forwardRef } from '@angular/core';
import { CalendarOptions, Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { FullCalendarComponent } from '@fullcalendar/angular';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import esLocale from '@fullcalendar/core/locales/es';

import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html'
})
export class CalendarioComponent implements OnInit {

    public evento:any = {};
    public calendarOptions?: CalendarOptions;
    eventsModel: any;

    @ViewChild('mevento')
    public meventoTemplate!: TemplateRef<any>;
    modalRef!: BsModalRef;

    constructor(private apiService: ApiService, private alertService: AlertService,  
        private route: ActivatedRoute, private router: Router,
        private modalService: BsModalService
    ){ }

    @ViewChild('fullcalendar') fullcalendar?: FullCalendarComponent;

    ngOnInit() {
        // need for load calendar bundle first
        forwardRef(() => Calendar);

        this.calendarOptions = {
            plugins: [interactionPlugin, dayGridPlugin, timeGridPlugin, listPlugin],
            editable: true,
            navLinks: true,
            firstDay: 0,
            timeZone: 'America/El_Salvador',
            locale: esLocale,
            themeSystem: 'bootstrap',
            businessHours: [ // specify an array instead
              {
                daysOfWeek: [ 1, 2, 3, 4, 5 ], // Monday, Tuesday, Wednesday
                startTime: '08:00', // 8am
                endTime: '17:00' // 5pm
              },
              {
                daysOfWeek: [ 6 ], // Thursday, Friday
                startTime: '08:00', // 10am
                endTime: '12:00' // 4pm
              }
            ],
            headerToolbar: {
              left: 'prev,next',
              center: 'title',
              right: 'today dayGridMonth,timeGridWeek,timeGridDay,listWeek'
            },
            customButtons: {
              myCustomButton: {
                text: 'Nuevo',
                click: this.handleDateClick.bind(this)
              }
            },
            dateClick: this.handleDateClick.bind(this),
            eventClick: this.handleEventClick.bind(this),
            eventChange: this.handleEventChange.bind(this),
            events: [
               {
                 id: '1',
                 title: 'Primer evento',
                 start: '2021-10-07',
                 end: '2021-10-07',
                 url: '',
                 allDay: true,
                 editable: true,
               },{
                 id: '2',
                 title: 'Segundo evento',
                 start: '2021-10-07T14:30:00',
                 end: '2021-10-07T15:30:00',
                 url: '',
                 allDay: false,
                 editable: true,
               }
             ]
        };
    }

    handleDateClick(arg:any) {
        this.evento = {};
        this.evento.start =  arg.dateStr;
        console.log(arg.dateStr);
        this.modalRef = this.modalService.show(this.meventoTemplate, {class: 'modal-sm'});
    }

    handleEventClick(arg:any) {
        this.evento = {};
        this.evento.title =  arg.event.title;
        this.evento.start =  arg.event.start;
        console.log(arg.event);
        this.modalRef = this.modalService.show(this.meventoTemplate, {class: 'modal-sm'});
    }

    handleEventChange(arg:any) {
        this.evento = {};
        console.log(arg.event);
        this.evento =  arg.event;
    }


    public onSubmit(){

    }

}