import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgChartsModule } from 'ng2-charts';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';


@Component({
  selector: 'app-dash-slider',
  templateUrl: './dash-slider.component.html'
})
export class DashSliderComponent implements OnInit {


	constructor( private alertService:AlertService, private apiService:ApiService
	) { }

	ngOnInit() {

  }

}
