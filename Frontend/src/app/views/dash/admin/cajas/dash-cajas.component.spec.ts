import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashCajasComponent } from './dash-cajas.component';

describe('DashCajasComponent', () => {
  let component: DashCajasComponent;
  let fixture: ComponentFixture<DashCajasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashCajasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashCajasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
