import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html'
})
export class ProductoComponent implements OnInit {

	public producto: any = {};
	public categorias:any[] = [];
    public loading = false;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router,
	) {
		this.router.routeReuseStrategy.shouldReuseRoute = function() {return false; };
	}

	ngOnInit() {
	    
	    this.loadAll();
	}

	loadAll(){
		const id = +this.route.snapshot.paramMap.get('id')!;
        
        this.producto = {};
        this.producto.tipo = 'Producto';
        this.producto.medida = 'Unidad';
        this.producto.tipo_impuesto = 'Gravada';
        this.producto.impuesto = 0.13;
        this.producto.empresa_id = this.apiService.auth_user().empresa_id;

        if (this.route.snapshot.queryParamMap.get('tipo')!) {
            this.producto.tipo = this.route.snapshot.queryParamMap.get('tipo')!;
            this.producto.precio = 0;
        }
		    
	    if(!isNaN(id)){
	        // Optenemos el producto
	        this.loading = true;
	        this.apiService.read('producto/', id).subscribe(producto => {
	            this.producto = producto;
	           	    
                // Rentabilidad
    				this.producto.utilidad = (this.producto.precio - this.producto.costo).toFixed(2);
    				this.producto.rentabilidad = ((this.producto.utilidad / this.producto.costo) * 100).toFixed(0);
                // Impuestos
                    this.producto.precio_total = ((this.producto.precio * 1) + (this.producto.precio * this.producto.impuesto)).toFixed(2);
                
                this.loading = false;
	        },error => {this.alertService.error(error);this.loading = false;});
	    }else{
	    	this.producto.codigo = Math.floor(Math.random() * (999999999 - 1 + 1)) + 1;
	    }

	}

	

}
