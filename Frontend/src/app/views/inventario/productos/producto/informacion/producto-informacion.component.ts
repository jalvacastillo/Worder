import { Component, OnInit, TemplateRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'app-producto-informacion',
  templateUrl: './producto-informacion.component.html'
})
export class ProductoInformacionComponent implements OnInit {

    @Input() producto: any = {};
    public categorias:any[] = [];
    public subcategorias:any[] = [];
    public categoria:any = {};
    public bodegas:any[] = [];
    public loading = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private route: ActivatedRoute, private router: Router,
    ) {
        this.router.routeReuseStrategy.shouldReuseRoute = function() {return false; };
    }

    ngOnInit() {
        

        this.apiService.getAll('categorias').subscribe(categorias => {
            this.categorias = categorias;

            if (this.producto.id) {
                this.categoria = this.categorias.find(item => item.id == this.producto.categoria_id);
                this.subcategorias = this.categoria.subcategorias;
            }

        }, error => {this.alertService.error(error);});
        this.apiService.getAll('bodegas').subscribe(bodegas => {
            this.bodegas = bodegas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false; });

    }

    public calPrecioBase(){
        this.producto.precio = (this.producto.precio_final / (1 + (this.producto.impuesto * 1))).toFixed(2);
    }

    public calPrecioFinal(){
        if (this.producto.tipo_impuesto == 'Gravada') {
            this.producto.impuesto = 0.13;
            this.producto.precio_final = ((this.producto.precio * 1) + (this.producto.precio * this.producto.impuesto)).toFixed(2);
        }else{
            this.producto.impuesto = 0;
        }
    }

    public setTipoComision(){
        if (this.producto.tipo_comision == 'Ninguna') {
            this.producto.comision = 0.0;
        }
    }

    public onSelectCategoria(categoria_id:any){
        this.categoria = this.categorias.find(item => item.id == categoria_id);
        this.subcategorias = this.categoria.subcategorias;
    }

    public setCategoria(categoria:any){
        this.categorias.push(categoria);
        this.producto.categoria_id = categoria.id;
    }

    public setSubCategoria(subcategoria:any){
        this.subcategorias.push(subcategoria);
        this.producto.subcategoria_id = subcategoria.id;
    }

    public onSubmit() {
        this.loading = true;
        this.apiService.store('producto', this.producto).subscribe(producto => {
            this.loading = false;
            if(!this.producto.id) {
                this.router.navigate(['/producto/'+ producto.id]);
            }
            this.producto = producto;
            this.alertService.success("Producto guardado");
        },error => {this.alertService.error(error); this.loading = false; });
    }

    public barcode(){
        var ventana = window.open(this.apiService.baseUrl + "/api/barcode/" + this.producto.codigo + "?token=" + this.apiService.auth_token(), "_new", "toolbar=yes, scrollbars=yes, resizable=yes, left=100, width=900, height=900");
    }

    

}
