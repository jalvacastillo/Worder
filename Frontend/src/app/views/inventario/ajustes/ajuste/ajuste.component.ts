import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'app-ajuste',
  templateUrl: './ajuste.component.html'
})
export class AjusteComponent implements OnInit {

	public ajuste: any = {};
 	public loading = false;

   constructor(private apiService: ApiService, private alertService: AlertService,  
    	private route: ActivatedRoute, private router: Router,
    	private modalService: BsModalService
    ){ }

	ngOnInit() {
        if (+this.route.snapshot.paramMap.get('id')!) {
            this.loadAll();
        }
	}

    public loadAll(){
        this.ajuste.id = +this.route.snapshot.paramMap.get('id')!;
        this.loading = true;
        this.apiService.read('ajuste/', this.ajuste.id).subscribe(ajuste => {
        this.ajuste = ajuste;
        this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});

    }

}
