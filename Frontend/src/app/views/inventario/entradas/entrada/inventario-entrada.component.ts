import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'app-inventario-entrada',
  templateUrl: './inventario-entrada.component.html'
})
export class InventarioEntradaComponent implements OnInit {

	public entrada: any = {};
	public detalle: any = {};

	public productos: any = [];
    public bodegas: any = [];
	public producto: any = {};

    public loading = false;
    modalRef!: BsModalRef;

	constructor( 
	    public apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router,
	    private modalService: BsModalService
    ) { 
        this.router.routeReuseStrategy.shouldReuseRoute = function() {return false; };
    }

    ngOnInit() {
        this.loadAll();
        this.loading = true;
        this.apiService.getAll('bodegas').subscribe(bodegas => {
            this.bodegas = bodegas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false; });

	}

	public loadAll(){
	    const id = +this.route.snapshot.paramMap.get('id')!;
	        
        if(!id){
    		this.entrada = {};
            this.entrada.fecha = this.apiService.date();
            this.entrada.usuario_id = this.apiService.auth_user().id;
            this.entrada.bodega_id = 1
            this.entrada.detalles = [];
        }
        else{
            // Optenemos el entrada
            this.loading = true;
            this.apiService.read('entrada/', id).subscribe(entrada => {
	            this.entrada = entrada;
            	this.loading = false;
            }, error => {this.alertService.error(error); this.loading = false; });
        }
	}


	openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    productoSelect(producto:any){
    	this.producto = producto;
        this.detalle.producto_id = this.producto.id;
        this.detalle.nombre_producto = this.producto.nombre;
        this.detalle.medida = this.producto.medida;
        this.detalle.costo = this.producto.costo;
        this.detalle.nombre_categoria = this.producto.nombre_categoria;
    	document.getElementById('cantidad')!.focus();
    }
	

	agregarDetalle(){
		this.detalle.total = this.detalle.cantidad * this.detalle.costo;
		this.entrada.detalles.push(this.detalle);
		this.producto = {};
		this.detalle = {};
        this.modalRef.hide();
	}

	public onSubmit() {
        this.loading = true;
        this.apiService.store('entrada', this.entrada).subscribe(entrada => {
            this.router.navigateByUrl('/entradas');
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false; });
    }

    openModalDetalle(template: TemplateRef<any>, detalle:any) {
        this.detalle = detalle;
        this.modalRef = this.modalService.show(template);
    }

    public editDetalle() {
        if(this.detalle.id) {
            this.loading = true;
    	    this.apiService.store('entrada/detalle', this.detalle).subscribe(data => {
    	    	this.detalle = {};
    			this.loading = false;
    		}, error => {this.alertService.error(error); this.loading = false; });
        }
        this.modalRef.hide();
	}


	public eliminarDetalle(detalle:any){
		if (confirm('¿Desea eliminar el Registro?')) {
			if(detalle.id) {
				this.apiService.delete('entrada/detalle/', detalle.id).subscribe(detalle => {
					for (var i = 0; i < this.entrada.detalles.length; ++i) {
						if (this.entrada.detalles[i].id === detalle.id ){
							this.entrada.detalles.splice(i, 1);
						}
					}
		        	this.alertService.success("Eliminado");
	        	}, error => {this.alertService.error(error); });
			}else{
				for (var i = 0; i < this.entrada.detalles.length; ++i) {
					if (this.entrada.detalles[i].producto_id === detalle.producto_id ){
						this.entrada.detalles.splice(i, 1);
					}
				}
	        	this.alertService.success("Eliminado");
			}
		}
	}


}
