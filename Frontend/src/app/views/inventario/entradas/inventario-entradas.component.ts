import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';

declare var $:any;

@Component({
  selector: 'app-inventario-entradas',
  templateUrl: './inventario-entradas.component.html',
})
export class InventarioEntradasComponent implements OnInit {

	public entradas:any = [];
    public buscador:any = '';

    public filtro:any = {};
    public filtrado:boolean = false;
    public usuarios:any = [];
    public loading:boolean = false;
    modalRef!: BsModalRef;

    constructor(public apiService: ApiService, private alertService: AlertService, 
        private modalService: BsModalService
    ){ }

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('entradas').subscribe(entradas => { 
            this.entradas = entradas;
            this.loading = false;
        }, error => {this.alertService.error(error); });
    }

    public search(){
    	if(this.buscador && this.buscador.length > 2) {
	    	this.apiService.read('entradas/buscar/', this.buscador).subscribe(entradas => { 
	    	    this.entradas = entradas;

	    	}, error => {this.alertService.error(error); });
    	}
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('entrada/', id) .subscribe(data => {
                for (let i = 0; i < this.entradas['data'].length; i++) { 
                    if (this.entradas['data'][i].id == data.id )
                        this.entradas['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.entradas.path + '?page='+ event.page).subscribe(entradas => { 
            this.entradas = entradas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    // Filtros
    openFilter(template: TemplateRef<any>) {

        if(!this.filtrado) {
            this.filtro.inicio = null;
            this.filtro.fin = null;
            this.filtro.usuario_id = '';
            this.filtro.estado = '';
            this.filtro.tipo = '';
        }
        if(!this.usuarios.data){
            this.apiService.getAll('usuarios/filtrar/tipo/Empleado').subscribe(usuarios => { 
                this.usuarios = usuarios.data;
            }, error => {this.alertService.error(error); });
        }
        this.modalRef = this.modalService.show(template);
    }

    onFiltrar(){
        this.loading = true;
        this.apiService.store('entradas/filtrar', this.filtro).subscribe(entradas => { 
            this.entradas = entradas;
            this.loading = false; this.filtrado = true;
            this.modalRef.hide();
        }, error => {this.alertService.error(error); this.loading = false;});

    }

}
