import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '@services/alert.service';
import { ApiService } from '@services/api.service';

declare var $:any;

@Component({
  selector: 'app-inventario-salidas',
  templateUrl: './inventario-salidas.component.html',
})
export class InventarioSalidasComponent implements OnInit {

	public salidas:any = [];
    public buscador:any = '';

    public filtro:any = {};
    public filtrado:boolean = false;
    public usuarios:any = [];
    public loading:boolean = false;
    modalRef!: BsModalRef;

    constructor(public apiService: ApiService, private alertService: AlertService, 
        private modalService: BsModalService
    ){ }

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('salidas').subscribe(salidas => { 
            this.salidas = salidas;
            this.loading = false;
        }, error => {this.alertService.error(error); });
    }

    public search(){
    	if(this.buscador && this.buscador.length > 2) {
	    	this.apiService.read('salidas/buscar/', this.buscador).subscribe(salidas => { 
	    	    this.salidas = salidas;

	    	}, error => {this.alertService.error(error); });
    	}
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('entrada/', id) .subscribe(data => {
                for (let i = 0; i < this.salidas['data'].length; i++) { 
                    if (this.salidas['data'][i].id == data.id )
                        this.salidas['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.salidas.path + '?page='+ event.page).subscribe(salidas => { 
            this.salidas = salidas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    // Filtros
    openFilter(template: TemplateRef<any>) {

        if(!this.filtrado) {
            this.filtro.inicio = null;
            this.filtro.fin = null;
            this.filtro.usuario_id = '';
            this.filtro.estado = '';
            this.filtro.tipo = '';
        }
        if(!this.usuarios.data){
            this.apiService.getAll('usuarios/filtrar/tipo/Empleado').subscribe(usuarios => { 
                this.usuarios = usuarios.data;
            }, error => {this.alertService.error(error); });
        }
        this.modalRef = this.modalService.show(template);
    }

    onFiltrar(){
        this.loading = true;
        this.apiService.store('salidas/filtrar', this.filtro).subscribe(salidas => { 
            this.salidas = salidas;
            this.loading = false; this.filtrado = true;
            this.modalRef.hide();
        }, error => {this.alertService.error(error); this.loading = false;});

    }

}
